/*
 * DepthGrabber.h
 *
 *  Created on: June 12, 2017
 *      Author: papazov
 */

#ifndef _CP_DEPTH_GRABBER_H_
#define _CP_DEPTH_GRABBER_H_

#include <depth/DepthSensor.h>
#include <depth/DepthUtils.h>
#include <png++/png.hpp>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstring>
#include <chrono>

namespace cp
{

class DepthGrabber: public DepthSensor::DepthFrameReceiver, DepthSensor::ColorFrameReceiver
{
protected:
  typedef png::basic_rgb_pixel<unsigned char> RGBPixel;

public:
  DepthGrabber() : m_save_confidence_map(true), m_should_save_stereo_camera_parameters(true){}
  virtual ~DepthGrabber(){}

  inline void run(const char* output_folder, bool save_confidence_map);

  inline void init_depth_processing(){ std::cout << "saving depth frames ...\n";}
  inline bool process_depth_frame(const DepthSense::DepthNode::NewSampleReceivedData& data);

  inline void init_color_processing(){ std::cout << "saving color frames ...\n";}
  inline bool process_color_frame(const DepthSense::ColorNode::NewSampleReceivedData& data);

protected:
  inline uint64_t get_stl_timestamp_ns();

protected:
  std::string m_output_folder;
  bool m_save_confidence_map;
  bool m_should_save_stereo_camera_parameters;

  std::ofstream m_timestamps_stl;
  std::ofstream m_timestamps_sdk;

  int m_depth_counter;
  int m_color_counter;
};

//=== inline methods ========================================================================================

inline void DepthGrabber::run(const char* output_folder, bool save_confidence_map)
{
  m_output_folder = output_folder;
  m_save_confidence_map = save_confidence_map;
  m_should_save_stereo_camera_parameters = true;

  // Open the STL timestamps table
  {
    // Construct the file name
    std::string stl_table_name(m_output_folder + "/timestamps_stl.csv");
    // Try to open the file
    m_timestamps_stl.open(stl_table_name);
    if (!m_timestamps_stl.is_open())
    {
      std::cerr << "Error: failed to open '" << stl_table_name << "'\n";
      return;
    }
  }

  // Open the SDK timestamps table
  {
    // Construct the file name
    std::string sdk_table_name(m_output_folder + "/timestamps_sdk.csv");
    // Try to open the file
    m_timestamps_sdk.open(sdk_table_name);
    if (!m_timestamps_sdk.is_open())
    {
      std::cerr << "Error: failed to open '" << sdk_table_name << "'\n";
      return;
    }
  }

  m_depth_counter = m_color_counter = 0;

  DepthSensor* depth_sensor = DepthSensor::Get(
    /*depth map=*/true,
    /*vertices=*/false,
    /*confidence map=*/m_save_confidence_map);
  depth_sensor->start(this, this);
}

//===========================================================================================================

inline uint64_t DepthGrabber::get_stl_timestamp_ns()
{
  auto time_point = std::chrono::steady_clock::now().time_since_epoch();
  auto ns = std::chrono::duration_cast<std::chrono::nanoseconds>(time_point).count();
  return static_cast<uint64_t>(ns);
}

//===========================================================================================================

inline bool DepthGrabber::process_depth_frame(const DepthSense::DepthNode::NewSampleReceivedData& data)
{
  // Get the time stamps
  uint64_t time_stamp_stl = this->get_stl_timestamp_ns();
  uint64_t time_stamp_sdk = 1000*data.timeOfCapture;

  if (!data.depthMap.size())
  {
    std::cerr << "Error: the sensor provides no depth values\n";
    return false;
  }

  // Get the width and height of the depth image
  int32_t w, h;
  DepthSense::FrameFormat_toResolution(data.captureConfiguration.frameFormat, &w, &h);

  // Save the depth map
  {
    // Get the raw pointer to the data
    const int16_t* depth_data = data.depthMap;
    // How many bits per depth value
    int nbits = 8*sizeof(depth_data[0]);

    // Create the depth file name
    std::stringstream depth_name;
    depth_name << "depth_frame_"
      << std::setfill('0') << std::setw(5) << m_depth_counter << "_"
      << nbits << "bit_mm_" << w << "x" << h << ".bin";

    // Open an output file
    std::string full_depth_name = m_output_folder + "/" + depth_name.str();
    std::ofstream depth_file(full_depth_name, std::ofstream::binary);

    if (!depth_file.is_open())
    {
      std::cerr << "Failed to open '" << full_depth_name << "'\n";
      return false;
    }

    // Write the depth data to file
    depth_file.write(reinterpret_cast<const char*>(depth_data), w*h*sizeof(depth_data[0]));
    depth_file.close();

    // Add a line to the tables
    if (m_timestamps_sdk.is_open()) m_timestamps_sdk << time_stamp_sdk << ", " << depth_name.str();
    if (m_timestamps_stl.is_open()) m_timestamps_stl << time_stamp_stl << ", " << depth_name.str();
  }

  // Shall we save the confidence map?
  if (data.confidenceMap.size())
  {
    // Get the raw pointer to the data
    const int16_t* confidence_map = data.confidenceMap;
    // How many bits per confidence value
    int nbits = 8*sizeof(confidence_map[0]);

    // Create the confidence file name
    std::stringstream confidence_name;
    confidence_name << "confidence_map_"
      << std::setfill('0') << std::setw(5) << m_depth_counter << "_"
      << nbits << "bit_" << w << "x" << h << ".bin";

    // Open an output file
    std::string full_confidence_name = m_output_folder + "/" + confidence_name.str();
    std::ofstream confidence_file(full_confidence_name, std::ofstream::binary);

    if (confidence_file.is_open())
    {
      // Write the depth data to file
      confidence_file.write(reinterpret_cast<const char*>(confidence_map), w*h*sizeof(confidence_map[0]));
      confidence_file.close();

      // Add a line to the tables
      if (m_timestamps_sdk.is_open()) m_timestamps_sdk << ", " << confidence_name.str();
      if (m_timestamps_stl.is_open()) m_timestamps_stl << ", " << confidence_name.str();
    }
  }

  // Add an end-of-line character to the tables
  if (m_timestamps_sdk.is_open()) m_timestamps_sdk << std::endl;
  if (m_timestamps_stl.is_open()) m_timestamps_stl << std::endl;

  // Prepare for the next iteration
  ++m_depth_counter;

  if (m_should_save_stereo_camera_parameters)
  {
    std::string params_file_name(m_output_folder + "/camera_parameters_depth_sense_sdk.txt");
    
    // Try to save the parameters
    if (!DepthUtils::save_stereo_camera_parameters(params_file_name, data.stereoCameraParameters))
      std::cerr << "Failed to save the stereo camera parameters\n";
      
    // We do not need to save the parameters any more
    m_should_save_stereo_camera_parameters = false;
  }

  // Keep going
  return true;
}

//===========================================================================================================

inline bool DepthGrabber::process_color_frame(const DepthSense::ColorNode::NewSampleReceivedData& data)
{
  // Get the time stamps
  uint64_t time_stamp_stl = this->get_stl_timestamp_ns();
  uint64_t time_stamp_sdk = 1000*data.timeOfCapture;

  // Get the width and height of the color image
  int32_t width, height;
  FrameFormat_toResolution(data.captureConfiguration.frameFormat, &width, &height);

  // Get the input pixel values
  const uint8_t *pixel_values = data.colorMap;

  // Create the output image
  png::image<RGBPixel> output_image(width, height);

  // Copy the pixel values to the output image
  for (int y = 0; y < height; ++y)
  {
    // Get the current row
    auto& dst_row = output_image.get_row(y);

    // Copy the pixel values to the row
    for (int x = 0; x < width; ++x, pixel_values += 3)
    {
      RGBPixel& dst_pixel = dst_row[x];
      dst_pixel.red = pixel_values[2];
      dst_pixel.green = pixel_values[1];
      dst_pixel.blue = pixel_values[0];
    }
  }

  // Create the image name
  std::stringstream image_name;
  image_name <<  "color_frame_" << std::setfill('0') << std::setw(5) << m_color_counter << ".png";
  std::string full_image_name = m_output_folder + "/" + image_name.str();

  // Open a stream for the image
  std::ofstream image_file(full_image_name, std::ofstream::binary);
  // Make sure we successfully opened the stream
  if (!image_file.is_open())
  {
    std::cerr << "Failed to open '" << full_image_name << "'\n";
    return false;
  }

  // Write the image
  output_image.write_stream(image_file);

  // Add a line to the tables
  if (m_timestamps_sdk.is_open()) m_timestamps_sdk << time_stamp_sdk << ", " << image_name.str() << std::endl;
  if (m_timestamps_stl.is_open()) m_timestamps_stl << time_stamp_stl << ", " << image_name.str() << std::endl;

  // Prepare for the next iteration
  ++m_color_counter;

  // Keep going
  return true;
}

//===========================================================================================================

} // namespace cp

#endif // _CP_DEPTH_GRABBER_H_
