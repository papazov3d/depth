/*
 * main.h
 *
 *  Created on: June 12, 2017
 *      Author: papazov
 */

#include "DepthGrabber.h"
#include <string>
#include <iostream>

//===========================================================================================================

void print_usage()
{
  std::cout << "usage:\ndepth_grabber <output folder> [options]\n\n"
    << "options:\n"
    << "--no-confidence-map: do not save the confidence values per depth pixel provided by the sensor\n";
}

//===========================================================================================================

bool contains_string(int argc, char *argv[], const std::string str)
{
  for (int i = 1; i < argc; ++i)
  {
    if (str == argv[i])
      return true;
  }

  return false;
}

//===========================================================================================================

std::string get_output_folder(int argc, char *argv[])
{
  for (int i = 1; i < argc; ++i)
  {
    std::string str(argv[i]);

    if (str != "--help" && str != "-h" && "--no-confidence-map")
      return str;
  }

  return "";
}

//===========================================================================================================

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    print_usage();
    return -1;
  }

  if (contains_string(argc, argv, "--help") || contains_string(argc, argv, "-h"))
  {
    print_usage();
    return -1;
  }

  std::string output_folder = get_output_folder(argc, argv);
  
  if (output_folder == "")
  {
    print_usage();
    return -1;
  }
  
  bool save_confidence_map = !contains_string(argc, argv, "--no-confidence-map");

  std::cout << "save confidence map: " << (save_confidence_map ? "yes" : "no") << std::endl;
  std::cout << "output folder:       " << output_folder << std::endl;
  std::cout << std::endl;
  
  cp::DepthGrabber depth_grabber;
  depth_grabber.run(output_folder.c_str(), save_confidence_map);

  return 0;
}

//===========================================================================================================
