/*
* PlaneFitterTest.cpp
*
*  Created on: May 26, 2017
*      Author: papazov
*/

#include "PlaneFitterTest.h"
#include <depth_vis/DepthVis.h>
#include <depth/PlaneFitter.h>
#include <iostream>

using namespace cp;

PlaneFitterTest::PlaneFitterTest()
: m_pixel_offsets(Point2i::compare),
  m_vtkwin(0, 0, 1800, 1500)
{
  m_vtkwin.get_render_window_interactor()->AddObserver(vtkCommand::KeyReleaseEvent, this);
}

//===========================================================================================================

PlaneFitterTest::~PlaneFitterTest()
{
  m_vtkwin.get_render_window_interactor()->RemoveObserver(this);
  this->SetReferenceCount(this->GetReferenceCount() - 1);
}

//===========================================================================================================

void PlaneFitterTest::Execute(vtkObject* caller, long unsigned int event_id, void* data)
{
  switch ( event_id )
  {
    case vtkCommand::KeyReleaseEvent:
    {
      vtkRenderWindowInteractor *inter = dynamic_cast<vtkRenderWindowInteractor*>(caller);
      if ( !inter )
        break;

      switch (inter->GetKeyCode())
      {
        case '1':
          m_vtk_mesh.toggle_visibility();
          break;
          
        case '2':
          m_vtk_points.toggle_visibility();
          break;

        case '4':
          m_vtk_plane.toggle_visibility();
          break;

        case 's':
          this->save_pixel_offsets();
          break;

        case '+':
        {
          size_t n = m_frame_averager.get_number_of_frames_to_average();
          if (n == 1)
            m_frame_averager.set_number_of_frames_to_average(10);
          else
            m_frame_averager.set_number_of_frames_to_average(n + 10);

          std::cout << "average over " << m_frame_averager.get_number_of_frames_to_average() << " frame(s)\n";
          break;
        }

        case '-':
        {
          size_t n = m_frame_averager.get_number_of_frames_to_average();
          if (n < 10)
            break;

          if (n == 10)
            m_frame_averager.set_number_of_frames_to_average(1);
          else
            m_frame_averager.set_number_of_frames_to_average(n - 10);
          
          std::cout << "average over " << m_frame_averager.get_number_of_frames_to_average() << " frame(s)\n";
          break;
        }

        case 32:
          this->grab_depth();
          break;
      }
    }
  }

  // Update the screen
  m_vtkwin.render();
}

//===========================================================================================================

void PlaneFitterTest::grab_depth()
{
  DepthSensor* depth_sensor = DepthSensor::Get(
    /*enable_depth_map=*/false,
    /*enable_vertices=*/false,
    /*enable_confidence_map=*/true,
    /*enable_floating_point_depth_map=*/true,
    /*enable_floating_point_vertices=*/true);

  std::cout << "collecting frames ...\n";
  depth_sensor->start(&m_frame_averager, /*rgb receiver=*/nullptr);
  std::cout << "averaged over " << m_frame_averager.get_number_of_frames() << " frame(s)\n";

  // Get the time-averaged frame
  const DepthFrame& avg_depth_frame = m_frame_averager.get_average_depth_frame();

  // Reconstruct the 3d points
  avg_depth_frame.reconstruct_3d_points(m_3d_points);

  // Fit the plane
  PlaneFitter::fit_plane_2d(m_3d_points, m_plane.m_a, m_plane.m_b, m_plane.m_c);

  // Compute and save the offset per pixel
  std::cout << "computing the pixel offsets ... "; std::cout << std::flush;
  this->collect_pixel_offsets(avg_depth_frame, m_plane);
  std::cout << "done\n";

  this->update_visualization();
}

//===========================================================================================================

void PlaneFitterTest::collect_pixel_offsets(const DepthFrame& depth_frame, const Plane2d& plane)
{
  // Loop over the depth image pixels and save the offsets
  for (int y = 0; y < depth_frame.get_height(); ++y)
  {
    for (int x = 0; x < depth_frame.get_width(); ++x)
    {
      DepthSense::FPVertex p;
      if (!depth_frame.reconstruct_3d_point(x, y, p))
        continue;

      float real_dist = std::sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
      p.x /= real_dist;
      p.y /= real_dist;
      p.z /= real_dist;
      float ideal_dist = plane.compute_intersection(p.x, p.y, p.z);

      // Get the map which saves the offsets for pixel (x, y)
      std::multimap<float, float>& dist2offset = m_pixel_offsets[Point2i(x, y)];
      // Insert the offset for the corresponding distance
      dist2offset.insert(std::pair<float,float>(real_dist, ideal_dist - real_dist));
    }
  }
}

//===========================================================================================================

void PlaneFitterTest::save_pixel_offsets()
{
  std::cout << "there are " << m_pixel_offsets.size() << " pixels\n";
  
  Point2i pixel(100, 100);
  const std::multimap<float, float>& offsets = m_pixel_offsets.find(pixel)->second;

  std::cout << "we have " << offsets.size() << " offsets for pixel ["
    << pixel.m_x << ", " << pixel.m_y << "]:\n";

  for (auto offset : offsets)
    std::cout << offset.first << ": " << offset.second << std::endl;
}

//===========================================================================================================

void PlaneFitterTest::update_visualization()
{
  const DepthFrame& avg_depth_frame = m_frame_averager.get_average_depth_frame();

  // Show the reconstructed 3d points
  DepthVis::get_points(m_3d_points, m_vtk_points.get_poly_data());

  // Get the bounding box of the points
  double box[6];
  m_vtk_points.get_poly_data()->ComputeBounds();
  m_vtk_points.get_poly_data()->GetBounds(box);

  // Get the plane parameters
  float a = m_plane.m_a;
  float b = m_plane.m_b;
  float c = m_plane.m_c;

  // Get 4 points at the corner of the depth image
  std::vector<DepthSense::FPVertex> corner_points(4);

  // Project the corner points of the depth image onto the plane
  corner_points[0].x = box[0];
  corner_points[0].y = box[2];
  corner_points[0].z = a*corner_points[0].x + b*corner_points[0].y + c;
  //
  corner_points[1].x = box[1];
  corner_points[1].y = box[2];
  corner_points[1].z = a*corner_points[1].x + b*corner_points[1].y + c;
  //
  corner_points[2].x = box[1];
  corner_points[2].y = box[3];
  corner_points[2].z = a*corner_points[2].x + b*corner_points[2].y + c;
  //
  corner_points[3].x = box[0];
  corner_points[3].y = box[3];
  corner_points[3].z = a*corner_points[3].x + b*corner_points[3].y + c;

  // Show the plane
  DepthVis::get_quad(corner_points, m_vtk_plane.get_poly_data());

  // Show the mesh
  DepthVis::get_mesh(m_frame_averager.get_average_depth_frame(), m_vtk_mesh.get_poly_data(), 0.1);
}

//===========================================================================================================

void PlaneFitterTest::run_test(int argc, char *argv[])
{
  m_frame_averager.set_number_of_frames_to_average(10);
  
  this->init_visualization();
  this->grab_depth();  
  
  // Start the visualization
  m_vtkwin.reset_view();
  m_vtkwin.start_main_loop();
}

//===========================================================================================================

void PlaneFitterTest::init_visualization()
{
  m_vtkwin.set_axes_length(0.25);
  m_vtkwin.parallel_projection_on();
  //m_vtkwin.axes_on();

  // The points reconstructed from the depth values
  m_vtk_points.set_representation_to_points();
  m_vtk_points.set_color(0, 0, 0.2);
  m_vtk_points.set_dot_size(5);
  m_vtk_points.visibility_off();
  m_vtkwin.add(m_vtk_points.get_actor());

  // The fitted plane
  m_vtk_plane.set_color(0.8, 0.2, 0);
  m_vtkwin.add(m_vtk_plane.get_actor());

  // The meshes
  m_vtk_mesh.set_color(0.0, 0.6, 1.0);
  m_vtkwin.add(m_vtk_mesh.get_actor());
}

//===========================================================================================================
