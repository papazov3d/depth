/*
* Reconstruct3dFromFile.cpp
*
*  Created on: June 14, 2017
*      Author: papazov
*/

#include "Reconstruct3dFromFile.h"
#include <depth_vis/DepthVis.h>
#include <depth/DepthUtils.h>
#include <iostream>

using namespace cp;

Reconstruct3dFromFile::Reconstruct3dFromFile()
: m_vtkwin(0, 0, 1800, 1500)
{
  m_vtkwin.get_render_window_interactor()->AddObserver(vtkCommand::KeyReleaseEvent, this);
}

//===========================================================================================================

Reconstruct3dFromFile::~Reconstruct3dFromFile()
{
  m_vtkwin.get_render_window_interactor()->RemoveObserver(this);
  this->SetReferenceCount(this->GetReferenceCount() - 1);
}

//===========================================================================================================

void Reconstruct3dFromFile::Execute(vtkObject* caller, long unsigned int event_id, void* data)
{
  switch ( event_id )
  {
    case vtkCommand::KeyReleaseEvent:
    {
      vtkRenderWindowInteractor *inter = dynamic_cast<vtkRenderWindowInteractor*>(caller);
      if ( !inter )
        break;

      switch (inter->GetKeyCode())
      {
        case '1':
          m_vtk_points.toggle_visibility();
          break;
      }
    }
  }

  // Update the screen
  m_vtkwin.render();
}

//===========================================================================================================

void Reconstruct3dFromFile::run_test(int argc, char *argv[])
{
  if (argc < 4)
  {
    std::cout << "depth <test nr.> <depth_file.bin> <stereo_parameters.txt>\n";
    return;
  }
  
  std::string depth_file_name = argv[2];
  std::string params_file_name = argv[3];

  // Load the stereo parameters
  DepthSense::StereoCameraParameters params;
  if (!DepthUtils::load_stereo_camera_parameters(params_file_name, params))
  {
    std::cerr << "Error: couldn't open parameters file '" << params_file_name << "'\n";
    return;
  }
  
  // Load the depth data
  std::vector<int16_t> depth_values;
  if (!DepthUtils::load_depth_data(depth_file_name, depth_values))
  {
    std::cerr << "Error: couldn't load the depth values from '" << depth_file_name << "'\n";
    return;
  }

  // Reconstruct the 3D points
  std::vector<DepthSense::Vertex> points_3d;
  DepthUtils::reconstruct_3d_points(depth_values, params, points_3d);

  // Show the points
  DepthVis::get_points(points_3d, m_vtk_points.get_poly_data(), /*max z coordinate=*/1500);

  std::cout << "Loaded " << depth_values.size() << " depth values\n";
  std::cout << "Reconstructed " << points_3d.size() << " points\n";
  DepthUtils::print_stereo_camera_parameters(params);

  this->init_visualization();
  
  // Start the visualization
  m_vtkwin.reset_view();
  m_vtkwin.start_main_loop();
}

//===========================================================================================================

void Reconstruct3dFromFile::init_visualization()
{
  m_vtkwin.set_axes_length(100);
  m_vtkwin.parallel_projection_on();
  m_vtkwin.axes_on();
  
  // The points reconstructed from the depth values
  m_vtk_points.set_representation_to_points();
  m_vtk_points.set_color(0, 0, 0.2);
  m_vtk_points.set_dot_size(5);
  m_vtkwin.add(m_vtk_points.get_actor());
}

//===========================================================================================================
