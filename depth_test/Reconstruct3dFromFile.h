/*
* Reconstruct3dFromFile.h
*
*  Created on: June 14, 2017
*      Author: papazov
*/

#ifndef _CP_RECONSTRUCT_3D_FROM_FILE_H_
#define _CP_RECONSTRUCT_3D_FROM_FILE_H_

#include <vtk/VtkWindow.h>
#include <vtk/VtkPolyModel.h>
#include <vtkCommand.h>

class Reconstruct3dFromFile: public vtkCommand
{
public:
  Reconstruct3dFromFile();
  virtual ~Reconstruct3dFromFile();

  void Execute(vtkObject* caller, long unsigned int event_id, void* data);

  void run_test(int argc, char *argv[]);

protected:
  void init_visualization();

protected:
  vtk::VtkPolyModel m_vtk_points;
  vtk::VtkWindow m_vtkwin;
};

#endif /* _CP_RECONSTRUCT_3D_FROM_FILE_H_ */
