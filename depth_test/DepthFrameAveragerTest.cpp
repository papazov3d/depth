/*
* DepthFrameAveragerTest.cpp
*
*  Created on: May 26, 2017
*      Author: papazov
*/

#include "DepthFrameAveragerTest.h"
#include <depth_vis/DepthVis.h>
#include <depth/PlaneFitter.h>
#include <iostream>

using namespace cp;

DepthFrameAveragerTest::DepthFrameAveragerTest()
: m_vtkwin(0, 0, 1800, 1500)
{
  m_vtkwin.get_render_window_interactor()->AddObserver(vtkCommand::KeyReleaseEvent, this);
}

//===========================================================================================================

DepthFrameAveragerTest::~DepthFrameAveragerTest()
{
  m_vtkwin.get_render_window_interactor()->RemoveObserver(this);
  this->SetReferenceCount(this->GetReferenceCount() - 1);
}

//===========================================================================================================

void DepthFrameAveragerTest::Execute(vtkObject* caller, long unsigned int event_id, void* data)
{
  switch ( event_id )
  {
    case vtkCommand::KeyReleaseEvent:
    {
      vtkRenderWindowInteractor *inter = dynamic_cast<vtkRenderWindowInteractor*>(caller);
      if ( !inter )
        break;

      switch (inter->GetKeyCode())
      {
        case '1':
          m_vtk_mesh1.toggle_visibility();
          break;
          
        case '2':
          m_vtk_mesh2.toggle_visibility();
          break;
          
        case '4':
          m_vtk_points.toggle_visibility();
          break;
          
        case '+':
        {
          size_t n = m_frame_averager.get_number_of_frames_to_average();
          if (n == 1)
            m_frame_averager.set_number_of_frames_to_average(10);
          else
            m_frame_averager.set_number_of_frames_to_average(n + 10);

          std::cout << "average over " << m_frame_averager.get_number_of_frames_to_average() << " frame(s)\n";
          break;
        }

        case '-':
        {
          size_t n = m_frame_averager.get_number_of_frames_to_average();
          if (n < 10)
            break;

          if (n == 10)
            m_frame_averager.set_number_of_frames_to_average(1);
          else
            m_frame_averager.set_number_of_frames_to_average(n - 10);
          
          std::cout << "average over " << m_frame_averager.get_number_of_frames_to_average() << " frame(s)\n";
          break;
        }

        case 32:
          this->grab_depth();
          break;
      }
    }
  }

  // Update the screen
  m_vtkwin.render();
}

//===========================================================================================================

void DepthFrameAveragerTest::grab_depth()
{
  DepthSensor* depth_sensor = DepthSensor::Get(
    /*enable_depth_map=*/false,
    /*enable_vertices=*/false,
    /*enable_confidence_map=*/true,
    /*enable_floating_point_depth_map=*/true,
    /*enable_floating_point_vertices=*/true);

  std::cout << "collecting frames ...\n";

  depth_sensor->start(&m_frame_averager, /*rgb receiver=*/nullptr);
  this->update_visualization(m_vtk_mesh1);
  std::cout << "averaged over first set (" << m_frame_averager.get_number_of_frames() << " frame(s))\n";

  depth_sensor->start(&m_frame_averager, /*rgb receiver=*/nullptr);
  this->update_visualization(m_vtk_mesh2);
  std::cout << "averaged over second set (" << m_frame_averager.get_number_of_frames() << " frame(s))\n";

  // Reconstruct the 3d points
  std::vector<DepthSense::FPVertex> points_3d;
  m_frame_averager.get_average_depth_frame().reconstruct_3d_points(points_3d);
  DepthVis::get_points(points_3d, m_vtk_points.get_poly_data());
}

//===========================================================================================================

void DepthFrameAveragerTest::update_visualization(vtk::VtkPolyModel& vtk_mesh)
{
  // Show the mesh
  DepthVis::get_mesh(m_frame_averager.get_average_depth_frame(), vtk_mesh.get_poly_data(), 0.1);
}

//===========================================================================================================

void DepthFrameAveragerTest::run_test(int argc, char *argv[])
{
  m_frame_averager.set_number_of_frames_to_average(10);
  
  this->grab_depth();  
  this->init_visualization();
  
  // Start the visualization
  m_vtkwin.reset_view();
  m_vtkwin.start_main_loop();
}

//===========================================================================================================

void DepthFrameAveragerTest::init_visualization()
{
  m_vtkwin.set_axes_length(0.25);
  m_vtkwin.parallel_projection_on();
  //m_vtkwin.axes_on();
  
  // The points reconstructed from the depth values
  m_vtk_points.set_representation_to_points();
  m_vtk_points.set_color(0, 0, 0.2);
  m_vtk_points.set_dot_size(5);
  m_vtk_points.visibility_off();
  m_vtkwin.add(m_vtk_points.get_actor());
  
  // The meshes
  m_vtk_mesh1.set_color(0.0, 0.6, 1.0);
  m_vtkwin.add(m_vtk_mesh1.get_actor());
  m_vtk_mesh2.set_color(0.9, 0.2, 0.0);
  m_vtkwin.add(m_vtk_mesh2.get_actor());
}

//===========================================================================================================
