/*
 * main.h
 *
 *  Created on: May 24, 2017
 *      Author: papazov
 */

#include "DepthSensorTest.h"
#include "DepthFrameAveragerTest.h"
#include "PlaneFitterTest.h"
#include "Reconstruct3dFromFile.h"
#include <cstdio>
#include <string>

void print_usage()
{
  printf("\nUsage:\ndepth <test number> [<input 1> <input 2> ...]\n\n"
    "Available tests:\n"
    //"1: DepthSensorTest\n"
    //"2: DepthFrameAveragerTest\n"
    //"3: PlaneFitterTest\n"
    "4: Reconstruct3DFromFile\n");
  printf("\n");
}

//===========================================================================================================

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    print_usage();
    return -1;
  }

  int test_number = std::atoi(argv[1]);

  switch (test_number)
  {
#if 0
    case 1:
    {
      DepthSensorTest test_case;
      test_case.run_test(argc, argv);
      break;
    }

    case 2:
    {
      DepthFrameAveragerTest test_case;
      test_case.run_test(argc, argv);
      break;
    }

    case 3:
    {
      PlaneFitterTest test_case;
      test_case.run_test(argc, argv);
      break;
    }
#endif
    case 4:
    {
      Reconstruct3dFromFile test_case;
      test_case.run_test(argc, argv);
      break;
    }

    default:
      printf("\nNo such test number: %i\n", test_number);
      print_usage();
      return -1;
  }

  return 0;
}

//===========================================================================================================
