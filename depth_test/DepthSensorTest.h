/*
* DepthSensorTest.h
*
*  Created on: May 24, 2017
*      Author: papazov
*/

#ifndef _CP_DEPTH_SENSOR_TEST_H_
#define _CP_DEPTH_SENSOR_TEST_H_

#include <depth/DepthSensor.h>

class DepthSensorTest: public cp::DepthSensor::DepthFrameReceiver
{
public:
  DepthSensorTest();
  virtual ~DepthSensorTest();

  void run_test(int argc, char *argv[]);

  void init_depth_processing();
  bool process_depth_frame(const DepthSense::DepthNode::NewSampleReceivedData& data);
};

#endif /* _CP_DEPTH_SENSOR_TEST_H_ */
