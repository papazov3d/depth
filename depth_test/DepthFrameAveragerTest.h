/*
* DepthFrameAveragerTest.h
*
*  Created on: May 26, 2017
*      Author: papazov
*/

#ifndef _CP_DEPTH_FRAME_AVERAGER_TEST_H_
#define _CP_DEPTH_FRAME_AVERAGER_TEST_H_

#include <depth/DepthFrameAverager.h>
#include <vtk/VtkWindow.h>
#include <vtk/VtkPolyModel.h>
#include <vtkCommand.h>

class DepthFrameAveragerTest: public vtkCommand
{
public:
  DepthFrameAveragerTest();
  virtual ~DepthFrameAveragerTest();

  void Execute(vtkObject* caller, long unsigned int event_id, void* data);

  void run_test(int argc, char *argv[]);

protected:
  void grab_depth();
  void update_visualization(vtk::VtkPolyModel& vtk_mesh);
  void init_visualization();

protected:
  cp::DepthFrameAverager m_frame_averager;
  vtk::VtkPolyModel m_vtk_points;
  vtk::VtkPolyModel m_vtk_mesh1, m_vtk_mesh2;
  vtk::VtkWindow m_vtkwin;
};

#endif /* _CP_DEPTH_FRAME_AVERAGER_TEST_H_ */
