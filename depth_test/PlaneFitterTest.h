/*
* PlaneFitterTest.h
*
*  Created on: Juni 8, 2017
*      Author: papazov
*/

#ifndef _CP_FRAME_FITTER_TEST_H_
#define _CP_FRAME_FITTER_TEST_H_

#include <depth/DepthFrameAverager.h>
#include <vtk/VtkPolyModel.h>
#include <vtk/VtkWindow.h>
#include <vtk/VtkPolyModel.h>
#include <vtkCommand.h>
#include <map>
#include <set>

class PlaneFitterTest: public vtkCommand
{
protected:
  class Plane2d
  {
  public:
    /** The method returns a real number t such that (t*u, t*v, t*w) ends up on this plane. If (u, v, w) 
     * has length one, t is the distance from the origin to this plane along (u, v, w). */
    inline float compute_intersection(float u, float v, float w) const;
    float m_a, m_b, m_c;
  };

  class Point2i
  {
  public:
    Point2i(int x, int y): m_x(x), m_y(y){}
    static bool compare(const Point2i& a, const Point2i& b)
    {
      if (a.m_x == b.m_x)
        return a.m_y < b.m_y;

      return a.m_x < b.m_x;
    }

    int m_x, m_y;
  };

public:
  PlaneFitterTest();
  virtual ~PlaneFitterTest();

  void Execute(vtkObject* caller, long unsigned int event_id, void* data);

  void run_test(int argc, char *argv[]);

protected:
  void grab_depth();
  void update_visualization();
  void init_visualization();
  void fit_and_show_plane();
  void collect_pixel_offsets(const cp::DepthFrame& depth_frame, const Plane2d& plane);
  void save_pixel_offsets();

protected:
  cp::DepthFrameAverager m_frame_averager;
  std::vector<DepthSense::FPVertex> m_3d_points;
  Plane2d m_plane;

  std::map<Point2i, std::multimap<float, float>, bool(*)(const Point2i&, const Point2i&)> m_pixel_offsets;

  vtk::VtkPolyModel m_vtk_points;
  vtk::VtkPolyModel m_vtk_plane;
  vtk::VtkPolyModel m_vtk_mesh;
  vtk::VtkWindow m_vtkwin;
};

//=== inline methods ========================================================================================

inline float PlaneFitterTest::Plane2d::compute_intersection(float u, float v, float w) const
{
  return m_c / (w - m_a*u - m_b*v);
}

//===========================================================================================================

#endif /* _CP_FRAME_FITTER_TEST_H_ */
