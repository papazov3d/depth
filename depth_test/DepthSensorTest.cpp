/*
* DepthSensorTest.cpp
*
*  Created on: May 24, 2017
*      Author: papazov
*/

#include "DepthSensorTest.h"
#include <iostream>

using namespace cp;
using namespace DepthSense;

DepthSensorTest::DepthSensorTest()
{
}

//===========================================================================================================

DepthSensorTest::~DepthSensorTest()
{
}

//===========================================================================================================

void DepthSensorTest::run_test(int argc, char *argv[])
{
  DepthSensor* depth_sensor = DepthSensor::Get();
  depth_sensor->start(this, /*rgb frame receiver=*/nullptr);
}

//===========================================================================================================

void DepthSensorTest::init_depth_processing()
{
  std::cout << "DepthSensorTest: initializing the depth processing\n";
}

//===========================================================================================================

bool DepthSensorTest::process_depth_frame(const DepthNode::NewSampleReceivedData& data)
{
  std::cout << "DepthSensorTest: got a depth frame\n";
  return false;
}

//===========================================================================================================
