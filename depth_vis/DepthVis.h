/*
 * DepthVis.h
 *
 *  Created on: May 29, 2017
 *      Author: papazov
 */

#ifndef _CP_DEPTH_VIS_H_
#define _CP_DEPTH_VIS_H_

#include "../depth/DepthFrame.h"
#include <vtkPolyData.h>
#include <limits>
#include <vector>

namespace cp
{

class DepthVis
{
public:
  /** Saves points and verts in 'dst'. */
  static void get_points(const std::vector<DepthSense::Vertex>& points, vtkPolyData* dst,
    int16_t max_z_coordinate = std::numeric_limits<int16_t>::max());

  /** Saves points and verts in 'dst'. */
  static void get_points(const std::vector<DepthSense::FPVertex>& points, vtkPolyData* dst);

  /** Connects the first 4 points in 'quad' to a quad and saves it in 'dst'. */
  static void get_quad(const std::vector<DepthSense::FPVertex>& quad, vtkPolyData* dst);

  static void get_vertices(const DepthFrame& src, vtkPolyData* dst);
  static void reconstruct_vertices(const DepthFrame& src, vtkPolyData* dst);

  /** Saves both the points and triangles from 'src' to 'dst'. */
  static void get_mesh(const DepthFrame& src, vtkPolyData* dst, float max_depth_difference);
  
  static void reconstruct_points(const DepthFrame& src, vtkPolyData* dst);
  static void get_triangles(const DepthFrame& src, vtkPolyData* dst, float max_depth_difference);
};

} // namespace cp

#endif // _CP_DEPTH_VIS_H_
