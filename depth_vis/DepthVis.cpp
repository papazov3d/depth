/*
 * DepthVis.cpp
 *
 *  Created on: May 29, 2017
 *      Author: papazov
 */

#include "DepthVis.h"
#include <vtkSmartPointer.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>

namespace cp
{

//===========================================================================================================

void DepthVis::get_points(const std::vector<DepthSense::Vertex>& points, vtkPolyData* dst,
  int16_t max_z_coordinate)
{
  vtkSmartPointer<vtkPoints> vtk_points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray> vtk_verts = vtkSmartPointer<vtkCellArray>::New();
  vtkIdType vtk_ids[1] = {0};

  for (auto& p : points)
  {
    if (p.z > max_z_coordinate)
      continue;

    vtk_points->InsertNextPoint(
      static_cast<float>(p.x),
      static_cast<float>(p.y),
      static_cast<float>(p.z));
    vtk_verts->InsertNextCell(1, vtk_ids);
    ++vtk_ids[0];
  }

  dst->SetPoints(vtk_points);
  dst->SetVerts(vtk_verts);
}

//===========================================================================================================

void DepthVis::get_points(const std::vector<DepthSense::FPVertex>& points, vtkPolyData* dst)
{
  vtkSmartPointer<vtkPoints> vtk_points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray> vtk_verts = vtkSmartPointer<vtkCellArray>::New();
  vtkIdType vtk_ids[1] = {0};

  for (auto& p : points)
  {
    vtk_points->InsertNextPoint(p.x, p.y, p.z);
    vtk_verts->InsertNextCell(1, vtk_ids);
    ++vtk_ids[0];
  }

  dst->SetPoints(vtk_points);
  dst->SetVerts(vtk_verts);
}

//===========================================================================================================

void DepthVis::get_quad(const std::vector<DepthSense::FPVertex>& quad, vtkPolyData* dst)
{
  if (quad.size() < 4)
    return;

  vtkSmartPointer<vtkPoints> vtk_points = vtkSmartPointer<vtkPoints>::New();
  vtk_points->InsertNextPoint(quad[0].x, quad[0].y, quad[0].z);
  vtk_points->InsertNextPoint(quad[1].x, quad[1].y, quad[1].z);
  vtk_points->InsertNextPoint(quad[2].x, quad[2].y, quad[2].z);
  vtk_points->InsertNextPoint(quad[3].x, quad[3].y, quad[3].z);

  vtkSmartPointer<vtkCellArray> vtk_quad = vtkSmartPointer<vtkCellArray>::New();
  vtkIdType vtk_ids[4] = {0, 1, 2, 3};  
  vtk_quad->InsertNextCell(4, vtk_ids);

  dst->SetPoints(vtk_points);
  dst->SetPolys(vtk_quad);
}

//===========================================================================================================

void DepthVis::get_vertices(const DepthFrame& src, vtkPolyData* dst)
{
  vtkSmartPointer<vtkPoints> vtk_points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray> vtk_verts = vtkSmartPointer<vtkCellArray>::New();
  vtkIdType vtk_ids[1] = {0};

  for (auto& vertex : src.get_vertices())
  {
    if (vertex.z > 200 || vertex.z <= 0)
      continue;
      
    vtk_points->InsertNextPoint(vertex.x, vertex.y, vertex.z);
    vtk_verts->InsertNextCell(1, vtk_ids);
    ++vtk_ids[0];
  }
  
  dst->SetPoints(vtk_points);
  dst->SetVerts(vtk_verts);
}

//===========================================================================================================

void DepthVis::reconstruct_vertices(const DepthFrame& src, vtkPolyData* dst)
{
  vtkSmartPointer<vtkPoints> vtk_points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray> vtk_verts = vtkSmartPointer<vtkCellArray>::New();
  vtkIdType vtk_ids[1] = {0};

  const float* depth_values = src.get_depth_values().data();
  int x, width = src.get_width();
  int y, height = src.get_height();

  DepthSense::ProjectionHelper projector(src.get_stereo_camera_parameters());
  DepthSense::FPExtended2DPoint p;
  DepthSense::FPVertex v;

  for (y = 0, p.point.y = 0; y < height; ++y, p.point.y += 1)
  {
    for (x = 0, p.point.x = 0; x < width; ++x, p.point.x += 1, ++depth_values)
    {
      if (*depth_values <= 0)
        continue;
        
      p.depth = *depth_values;
      projector.get3DCoordinates(&p, &v, /*#points=*/1);
      vtk_points->InsertNextPoint(v.x, v.y, v.z);
      vtk_verts->InsertNextCell(1, vtk_ids);
      ++vtk_ids[0];
    }
  }

  dst->SetPoints(vtk_points);
  dst->SetVerts(vtk_verts);
}

//===========================================================================================================

void DepthVis::reconstruct_points(const DepthFrame& src, vtkPolyData* dst)
{
  vtkSmartPointer<vtkPoints> vtk_points = vtkSmartPointer<vtkPoints>::New();

  const float* depth_values = src.get_depth_values().data();
  int x, width = src.get_width();
  int y, height = src.get_height();

  DepthSense::ProjectionHelper projector(src.get_stereo_camera_parameters());
  DepthSense::FPExtended2DPoint p;
  DepthSense::FPVertex v;

  for (y = 0, p.point.y = 0; y < height; ++y, p.point.y += 1)
  {
    for (x = 0, p.point.x = 0; x < width; ++x, p.point.x += 1, ++depth_values)
    {
      if (*depth_values <= 0)
        continue;

      p.depth = *depth_values;
      projector.get3DCoordinates(&p, &v, /*#points=*/1);
      vtk_points->InsertNextPoint(v.x, v.y, v.z);
    }
  }

  dst->SetPoints(vtk_points);
}

//===========================================================================================================

void DepthVis::get_triangles(const DepthFrame& src, vtkPolyData* dst, float max_depth_difference)
{
  vtkSmartPointer<vtkCellArray> vtk_trias = vtkSmartPointer<vtkCellArray>::New();
  vtkIdType vtk_ids[3];

  const float* depth_values = src.get_depth_values().data();
  vtkIdType width = static_cast<vtkIdType>(src.get_width());
  vtkIdType height = static_cast<vtkIdType>(src.get_height());

  for (vtkIdType y = 0; y < height - 1; ++y)
  {
    for (vtkIdType x = 0; x < width - 1; ++x)
    {
      float d1 = src.get_depth_value(x, y);

      if (d1 <= 0)
        continue;

      float d2 = src.get_depth_value(x + 1, y);
      float d3 = src.get_depth_value(x + 1, y + 1);
      float d4 = src.get_depth_value(x, y + 1);
      
      if (std::fabs(d1-d2) > max_depth_difference || std::fabs(d1-d3) > max_depth_difference ||
          std::fabs(d1-d4) > max_depth_difference || std::fabs(d2-d3) > max_depth_difference ||
          std::fabs(d2-d4) > max_depth_difference || std::fabs(d3-d4) > max_depth_difference)
        continue;
      
      // The lower left triangle
      vtk_ids[0] = y*width + x;
      vtk_ids[1] = vtk_ids[0] + 1;
      vtk_ids[2] = vtk_ids[0] + width;
      vtk_trias->InsertNextCell(3, vtk_ids);
      
      // The upper right triangle
      vtk_ids[0] = y*width + x + 1;
      vtk_ids[1] = vtk_ids[0] + width;
      vtk_ids[2] = vtk_ids[1] - 1;
      vtk_trias->InsertNextCell(3, vtk_ids);
    }
  }

  dst->SetPolys(vtk_trias);
}

//===========================================================================================================

void DepthVis::get_mesh(const DepthFrame& src, vtkPolyData* dst, float max_depth_difference)
{
  DepthVis::reconstruct_points(src, dst);
  DepthVis::get_triangles(src, dst, max_depth_difference);
}

//===========================================================================================================

} // namespace cp
