/*
 * VtkWindow.h
 *
 *  Created on: Jul 21, 2013
 *      Author: papazov
 */

#ifndef VTKWINDOW_H_
#define VTKWINDOW_H_

#include <vtkCamera.h>
#include <vtkCommand.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyle.h>
#include <vtkAxesActor.h>
#include <vtkProp.h>
#include <vector>

namespace vtk
{

class VtkWindow: public vtkCommand
{
public:
  VtkWindow(int posx = 0, int posy = 0, int width = 800, int height = 600);
  virtual ~VtkWindow();

  /** Inherited from 'vtkCommand'. */
  void Execute(vtkObject *caller, unsigned long event_id, void *data);

  /** Adds the 'actor' to the indicated renderer (default is the first one) such that it will be displayed by
  the next rendering. */
  void add(vtkProp* actor, int which_renderer = 0){ m_renderers[which_renderer]->AddActor(actor); }

  /** Adds the 'actor' to the indicated renderer (default is the first one) such that it will be displayed by
  the next rendering. */
  void add_2d(vtkProp* actor, int which_renderer = 0){ m_renderers[which_renderer]->AddActor2D(actor); }

  /** Removes the 'actor' from the indicated renderer renderer. */
  void remove(vtkProp* actor, int which_renderer = 0){ m_renderers[which_renderer]->RemoveActor(actor); }

  void add_renderer();
  size_t get_number_of_renderers() const { return m_renderers.size(); }

  void render();
  void reset_view();
  void start_main_loop(){ this->render(); m_interactor->Start();}

  void perspective_projection_on();
  void perspective_projection_off();
  void parallel_projection_on();
  void parallel_projection_off();
  void toggle_projection_type();

  void toggle_axes();
  void toggle_axes_off(){ m_toggle_axes = false; }
  void toggle_axes_on(){ m_toggle_axes = true; }
  bool get_toggle_axes() const { return m_toggle_axes; }
  void axes_off();
  void axes_on();
  void set_axes_length(double length);

  vtkRenderWindowInteractor* get_render_window_interactor(){ return m_interactor;}
  vtkRenderWindow* get_render_window(){ return m_render_window;}
  vtkRenderer* get_renderer(int which_renderer = 0){ return m_renderers[which_renderer]; }
  vtkCamera* get_camera(int which_camera = 0){ return m_renderers[which_camera]->GetActiveCamera(); }
  const int* get_window_size() const { return m_render_window->GetSize();}

  void set_camera_position(double x, double y, double z){ this->get_camera()->SetPosition(x, y, z);}
  void set_camera_focal_point(double x, double y, double z){ this->get_camera()->SetFocalPoint(x, y, z);}
  void set_camera_view_up(double x, double y, double z){ this->get_camera()->SetViewUp(x, y, z);}
  void set_camera_view_angle(double angle){ this->get_camera()->SetViewAngle(angle);}

  void set_window_size(int width, int height){ m_render_window->SetSize(width, height);}
  void set_window_position(int x, int y){ m_render_window->SetPosition(x, y);}

  void print_camera_parameters();
  void print_camera_parameters_on(){ m_print_camera_parameters = true;}
  void print_camera_parameters_off(){ m_print_camera_parameters = false;}

  bool get_align_camera_to_axes() const { return m_align_camera_to_axes; }
  void align_camera_to_axes_on(){ m_align_camera_to_axes = true; }
  void align_camera_to_axes_off(){ m_align_camera_to_axes = false; }

protected:
  std::vector<vtkRenderer*> m_renderers;
  vtkRenderWindow *m_render_window;
  vtkRenderWindowInteractor *m_interactor;

  vtkInteractorStyle *m_interactor_style;
  std::vector<vtkAxesActor*> m_axes;

  bool m_print_camera_parameters;
  bool m_align_camera_to_axes;
  bool m_toggle_axes;
};

} /* namespace vtk */

#endif /* VTKWINDOW_H_ */
