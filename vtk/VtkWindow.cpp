/*
 * VtkWindow.cpp
 *
 *  Created on: Jul 21, 2013
 *      Author: papazov
 */

#include "VtkWindow.h"
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkRendererCollection.h>

namespace vtk
{

VtkWindow::VtkWindow(int posx, int posy, int width, int height)
: m_render_window(vtkRenderWindow::New()),
  m_interactor(vtkRenderWindowInteractor::New()),
  m_interactor_style(vtkInteractorStyleTrackballCamera::New()),
  m_print_camera_parameters(true),
  m_align_camera_to_axes(true),
  m_toggle_axes(true)
{
  // Create and setup a default renderer
  m_renderers.push_back(vtkRenderer::New());
  m_renderers[0]->SetBackground(1,1,1);

  // Setup the render window
  m_render_window->SetPosition(posx, posy);
  m_render_window->SetSize(width, height);
  m_render_window->AddRenderer(m_renderers[0]);

  // Set the render window interactor
  m_interactor->SetRenderWindow(m_render_window);
  m_interactor->SetInteractorStyle(m_interactor_style);
  // Register yourself as an observer by the render window interactor
  m_interactor->AddObserver(vtkCommand::KeyReleaseEvent, this, 0.0/*priority*/);

  // Setup the axes
  m_axes.push_back(vtkAxesActor::New());
  m_axes[0]->AxisLabelsOff();
  m_renderers[0]->AddActor(m_axes[0]);

  this->axes_off();
}

VtkWindow::~VtkWindow()
{
  m_interactor->Delete();
  m_interactor_style->Delete();
  m_render_window->Delete();

  for (auto it = m_renderers.begin(); it != m_renderers.end(); ++it)
    (*it)->Delete();

  for (auto it = m_axes.begin(); it != m_axes.end(); ++it)
    (*it)->Delete();

  this->SetReferenceCount(this->GetReferenceCount() - 1);
}

//============================================================================================================

void VtkWindow::Execute(vtkObject *caller, unsigned long event_id, void *data)
{
  vtkRenderWindowInteractor *renwi = dynamic_cast<vtkRenderWindowInteractor*>(caller);

  if ( !renwi )
    return;

  switch ( event_id )
  {
    case vtkCommand::KeyReleaseEvent:
    {
      switch ( renwi->GetKeyCode() )
      {
        case 27: // Esc
          renwi->TerminateApp();
          break;

        case 'c':
          if (m_toggle_axes)
            this->toggle_axes();
          break;

        case 'o':
          this->toggle_projection_type();
          break;

        case 'p':
          if ( m_print_camera_parameters )
            this->print_camera_parameters();
          break;

        case 'r':
          this->reset_view();
          this->render();
          break;

        case 'x':
        case 'y':
        case 'z':
        {
          if (!m_align_camera_to_axes)
            break;

          vtkCamera* cam = renwi->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera();

          double campos[3], fpos[3];
          cam->GetPosition(campos);
          cam->GetFocalPoint(fpos);

          double diff[3] = {campos[0]-fpos[0], campos[1]-fpos[1], campos[2]-fpos[2]};
          double dist = sqrt(diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2]);

          if ( renwi->GetKeyCode() == 'x' )
          {
            if ( diff[0] < 0 ) fpos[0] -= dist;
            else fpos[0] += dist;
          }
          else if ( renwi->GetKeyCode() == 'y' )
          {
            if ( diff[1] < 0 ) fpos[1] -= dist;
            else fpos[1] += dist;
          }
          else // renwi->GetKeyCode() == 'z'
          {
            if ( diff[2] < 0 ) fpos[2] -= dist;
            else fpos[2] += dist;
          }

          // Update the camera position such that the viewing direction is aligned with one of the global axes
          cam->SetPosition(fpos);
          renwi->Render();

          break;
        }
      }

      break;
    }
  }
}

//============================================================================================================

void VtkWindow::add_renderer()
{
  // Create a new renderer
  vtkRenderer* new_renderer = vtkRenderer::New();
  new_renderer->SetBackground(1.0, 1.0, 1.0);
  m_renderers.push_back(new_renderer);
  m_render_window->AddRenderer(new_renderer);

  // Add axes to the new renderer
  vtkAxesActor* new_axes = vtkAxesActor::New();
  new_axes->AxisLabelsOff();
  new_axes->SetVisibility(m_axes[0]->GetVisibility());
  m_axes.push_back(new_axes);
  new_renderer->AddActor(new_axes);

  double width_step = 1.0 / this->get_number_of_renderers();
  double min = 0.0, max = width_step;

  for (auto it = m_renderers.begin(); it != m_renderers.end(); ++it, min += width_step, max += width_step)
    (*it)->SetViewport(min, 0.0, max, 1.0);

  this->render();
}

//============================================================================================================

void VtkWindow::print_camera_parameters()
{
  double campos[3], viewup[3], fpos[3];
  vtkCamera* cam = m_renderers[0]->GetActiveCamera();
  cam->GetPosition(campos);
  cam->GetFocalPoint(fpos);
  cam->GetViewUp(viewup);
  int *size = m_render_window->GetSize();

  printf("\ncamera parameters:\n"
      "m_vtkwin.set_camera_position(%lf, %lf, %lf);\n"
      "m_vtkwin.set_camera_focal_point(%lf, %lf, %lf);\n"
      "m_vtkwin.set_camera_view_up(%lf, %lf, %lf);\n"
      "m_vtkwin.set_camera_view_angle(%lf);\n"
      "m_vtkwin.set_window_size(%i, %i);\n\n",
       campos[0], campos[1], campos[2], fpos[0], fpos[1], fpos[2],
       viewup[0], viewup[1], viewup[2],
       cam->GetViewAngle(), size[0], size[1]);
}

//============================================================================================================

void VtkWindow::toggle_axes()
{
  for (auto it = m_axes.begin(); it != m_axes.end(); ++it)
    (*it)->SetVisibility(!(*it)->GetVisibility());

  this->render();
}

//============================================================================================================

void VtkWindow::axes_off()
{
  for (auto it = m_axes.begin(); it != m_axes.end(); ++it)
    (*it)->SetVisibility(0);
  
  m_render_window->Render();
}

//============================================================================================================

void VtkWindow::axes_on()
{
  for (auto it = m_axes.begin(); it != m_axes.end(); ++it)
    (*it)->SetVisibility(1);
  
  this->render();
}

//============================================================================================================

void VtkWindow::set_axes_length(double length)
{
  for (auto it = m_axes.begin(); it != m_axes.end(); ++it)
    (*it)->SetTotalLength(length, length, length);
}

//============================================================================================================

void VtkWindow::render()
{
  for (auto it = m_renderers.begin(); it != m_renderers.end(); ++it)
    (*it)->ResetCameraClippingRange();

  m_render_window->Render();
}

//============================================================================================================

void VtkWindow::reset_view()
{
  for (auto it = m_renderers.begin(); it != m_renderers.end(); ++it)
    (*it)->ResetCamera();
}

//============================================================================================================

void VtkWindow::perspective_projection_on()
{
  for (auto it = m_renderers.begin(); it != m_renderers.end(); ++it)
    (*it)->GetActiveCamera()->ParallelProjectionOff();
  
  this->render();
}

//============================================================================================================

void VtkWindow::perspective_projection_off()
{
  for (auto it = m_renderers.begin(); it != m_renderers.end(); ++it)
    (*it)->GetActiveCamera()->ParallelProjectionOn();
  
  this->render();
}

//============================================================================================================

void VtkWindow::parallel_projection_on()
{
  for (auto it = m_renderers.begin(); it != m_renderers.end(); ++it)
    (*it)->GetActiveCamera()->ParallelProjectionOn();
  
  this->render();
}

//============================================================================================================

void VtkWindow::parallel_projection_off()
{
  for (auto it = m_renderers.begin(); it != m_renderers.end(); ++it)
    (*it)->GetActiveCamera()->ParallelProjectionOff();
  
  this->render();
}

//============================================================================================================

void VtkWindow::toggle_projection_type()
{
  for (auto it = m_renderers.begin(); it != m_renderers.end(); ++it)
    (*it)->GetActiveCamera()->SetParallelProjection(!(*it)->GetActiveCamera()->GetParallelProjection());

  this->render();
}

//============================================================================================================

} /* namespace vtk */
