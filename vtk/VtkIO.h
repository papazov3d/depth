/*
 * VtkIO.h
 *
 *  Created on: Dec 27, 2013
 *      Author: papazov
 */

#ifndef VTKIO_H_
#define VTKIO_H_

#include <vtkPolyData.h>

namespace vtk
{

class VtkIO
{
public:
  VtkIO(){}
  virtual ~VtkIO(){}

  /** Loads the data from the file named 'file_name' and copies it to 'out'. Supported file types are vtk, ply, obj. */
  static bool load(const char* file_name, vtkPolyData* out);

  /** Saves the 'model' on disk. If 'file_name' is 0, the method asks the user (via scanf) to type in a file name. */
  static bool save(vtkPolyData* model, const char* file_name = 0);

  /** Returns a char-array containing the formats supported by this class. */
  static const char* get_list_of_supported_file_types(){ return m_list_of_supported_3d_formats;}

protected:
  static char m_list_of_supported_3d_formats[2048];
};

} /* namespace vtk */

#endif /* VTKIO_H_ */
