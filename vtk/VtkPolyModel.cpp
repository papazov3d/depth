/*
 * VtkPolyModel.cpp
 *
 *  Created on: Jul 21, 2013
 *      Author: papazov
 */

#include "VtkPolyModel.h"
#include <vtkPolyDataNormals.h>
#include <vtkColorTransferFunction.h>
#include <vtkSmartPointer.h>
#include <vtkLookupTable.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkMath.h>

namespace vtk
{

VtkPolyModel::VtkPolyModel()
: m_mapper(vtkPolyDataMapper::New()),
  m_actor(vtkActor::New())
{
  vtkPolyData* poly_data = vtkPolyData::New();
  m_mapper->SetInputData(poly_data);
  m_actor->SetMapper(m_mapper);

  poly_data->Delete();

  this->set_interpolation_to_flat();
}

VtkPolyModel::VtkPolyModel(vtkAlgorithmOutput* poly_data_src)
: m_mapper(vtkPolyDataMapper::New()),
  m_actor(vtkActor::New())
{
  m_mapper->SetInputConnection(poly_data_src);
  m_actor->SetMapper(m_mapper);

  this->set_interpolation_to_flat();
}

VtkPolyModel::~VtkPolyModel()
{
  m_actor->Delete();
  m_mapper->Delete();
}

//======================================================================================================================

void VtkPolyModel::reset()
{
  vtkPolyData* poly_data = vtkPolyData::New();
  m_mapper->SetInputData(poly_data);
  poly_data->Delete();
}

//======================================================================================================================

void VtkPolyModel::add_own_points(vtkIdType num_points)
{
  vtkPolyData* poly_data = this->get_poly_data();

  if ( poly_data->GetPoints() )
    return;

  // Create the points
  vtkPoints* points = vtkPoints::New();
  if ( num_points )
    points->SetNumberOfPoints(num_points);

  // Save them in the poly data
  poly_data->SetPoints(points);

  // Cleanup
  points->Delete();
}

//======================================================================================================================

void VtkPolyModel::compute_triangle_normals(vtkPolyData* out)
{
  vtkPoints* out_points = vtkPoints::New(VTK_DOUBLE);
  vtkCellArray* out_lines = vtkCellArray::New();

  // Get the triangles
  vtkPoints* in_points = this->get_points();
  vtkCellArray* in_trias = this->get_poly_data()->GetPolys();
  vtkIdList* tria_vertex_ids = vtkIdList::New();
  vtkIdType ids[2] = {0, 1};
  double p0[3], p1[3], p2[3];

  // Loop over the triangles
  for (in_trias->InitTraversal(); in_trias->GetNextCell(tria_vertex_ids); tria_vertex_ids->Reset())
  {
    if (tria_vertex_ids->GetNumberOfIds() != 3)
      continue;
      
    // Get the triangle points
    in_points->GetPoint(tria_vertex_ids->GetId(0), p0);
    in_points->GetPoint(tria_vertex_ids->GetId(1), p1);
    in_points->GetPoint(tria_vertex_ids->GetId(2), p2);

    // Compute the perimeter
    double avg_edge_len = (
      std::sqrt(vtkMath::Distance2BetweenPoints(p0, p1)) +
      std::sqrt(vtkMath::Distance2BetweenPoints(p1, p2)) +
      std::sqrt(vtkMath::Distance2BetweenPoints(p2, p0))
      )/4.0;

    // Compute the normal
    double u[3] = { p1[0] - p0[0], p1[1] - p0[1], p1[2] - p0[2] };
    double v[3] = { p2[0] - p0[0], p2[1] - p0[1], p2[2] - p0[2] };
    double n[3];
    vtkMath::Cross(u, v, n);
    vtkMath::Normalize(n);
    vtkMath::MultiplyScalar(n, avg_edge_len);

    // Compute the centroid (the base point)
    double c[3];
    c[0] = (p0[0] + p1[0] + p2[0]) / 3.0;
    c[1] = (p0[1] + p1[1] + p2[1]) / 3.0;
    c[2] = (p0[2] + p1[2] + p2[2]) / 3.0;

    // Compute the tip of the normal
    double normal_tip[3];
    vtkMath::Add(c, n, normal_tip);

    // Save both points
    out_points->InsertNextPoint(c);
    out_points->InsertNextPoint(normal_tip);

    // Insert the lines
    out_lines->InsertNextCell(2, ids);
    ids[0] += 2;
    ids[1] += 2;
  }
  
  // Save the result
  out->SetPoints(out_points);
  out->SetLines(out_lines);

  // Clean up
  out_points->Delete();
  out_lines->Delete();
  tria_vertex_ids->Delete();
}

//======================================================================================================================

void VtkPolyModel::set_color_mode_to_map_point_scalars_to_hue(const std::vector<ScalarHuePair>& scalar_hue_pairs)
{
  this->set_color_mode_to_map_scalars_to_hue(scalar_hue_pairs);
  this->get_mapper()->SetScalarModeToUsePointData();
}

//======================================================================================================================

void VtkPolyModel::set_color_mode_to_map_cell_scalars_to_hue(const std::vector<ScalarHuePair>& scalar_hue_pairs)
{
  this->set_color_mode_to_map_scalars_to_hue(scalar_hue_pairs);
  this->get_mapper()->SetScalarModeToUseCellData();
}

//======================================================================================================================

void VtkPolyModel::set_color_mode_to_map_scalars_to_hue(const std::vector<ScalarHuePair>& scalar_hue_pairs)
{
  vtkSmartPointer<vtkColorTransferFunction> color_transfer = vtkSmartPointer<vtkColorTransferFunction>::New();
  color_transfer->SetColorSpaceToHSV();
  color_transfer->SetScaleToLinear();

  for (auto it = scalar_hue_pairs.begin(); it != scalar_hue_pairs.end(); ++it)
    color_transfer->AddHSVPoint(/*scalar*/it->first, /*hue*/it->second, /*saturation*/1, /*value*/1);
  
  color_transfer->ClampingOn();
  color_transfer->Build();
  
  this->get_mapper()->SetLookupTable(color_transfer);
  this->get_mapper()->SetColorModeToMapScalars();
}

//======================================================================================================================

void VtkPolyModel::set_interpolation_to_gouraud()
{
  vtkPolyData* poly_data = this->get_poly_data();

  // Compute normals if necessary
  if ( poly_data->GetPointData()->GetNormals() == 0 )
  {
    vtkPolyDataNormals* normals_filter = vtkPolyDataNormals::New();
      normals_filter->SplittingOff();
      normals_filter->SetInputData(poly_data);
      normals_filter->Update();
    poly_data->DeepCopy(normals_filter->GetOutput());
    normals_filter->Delete();
  }

  m_actor->GetProperty()->SetInterpolationToGouraud();
  poly_data->Modified();
}

//======================================================================================================================

void VtkPolyModel::set_interpolation_to_phong()
{
  vtkPolyData* poly_data = this->get_poly_data();

  // Compute normals if necessary
  if ( poly_data->GetPointData()->GetNormals() == 0 )
  {
    vtkPolyDataNormals* normals_filter = vtkPolyDataNormals::New();
      normals_filter->SplittingOff();
      normals_filter->SetInputData(poly_data);
      normals_filter->Update();
    poly_data->DeepCopy(normals_filter->GetOutput());
    normals_filter->Delete();
  }

  m_actor->GetProperty()->SetInterpolationToPhong();
  poly_data->Modified();
}

//======================================================================================================================

} /* namespace vtk */
