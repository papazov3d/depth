/*
 * VtkIO.cpp
 *
 *  Created on: Dec 27, 2013
 *      Author: papazov
 */

#include "VtkIO.h"
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkPLYReader.h>
#include <vtkOBJReader.h>
#include <vtkErrorCode.h>
#include <cstdio>

namespace vtk
{

char VtkIO::m_list_of_supported_3d_formats[2048] = "vtk, ply, obj";

//=============================================================================================================

bool VtkIO::load(const char* file_name, vtkPolyData* out)
{
  // First of all check if the file exists
  FILE* tmp_fp = fopen(file_name, "r");
  if (!tmp_fp)
  {
    printf("ERROR in 'VtkIO::load()': couldn't open '%s'.\n", file_name);
    return false;
  }
  else
    fclose(tmp_fp);

  int len = (int)strlen(file_name);

  if (len < 3)
  {
    printf("ERROR in 'VtkIO::load()': could not get the file extension (file name too short).\n");
    return false;
  }

  // Get the file extension
  char file_ext[4] = { file_name[len - 3], file_name[len - 2], file_name[len - 1], '\0' };
  bool success = false;

  if (strcmp("vtk", file_ext) == 0 || strcmp("VTK", file_ext) == 0)
  {
    vtkPolyDataReader* reader = vtkPolyDataReader::New();
    reader->SetFileName(file_name);
    reader->Update();

    if (reader->IsFilePolyData())
    {
      out->DeepCopy(reader->GetOutput());
      success = true;
    }
    
    reader->Delete();
  }
  else if (strcmp("ply", file_ext) == 0 || strcmp("PLY", file_ext) == 0)
  {
    vtkPLYReader* reader = vtkPLYReader::New();
    reader->SetFileName(file_name);
    reader->Update();
    vtkPolyData* model = reader->GetOutput();

    if (reader->GetErrorCode() == vtkErrorCode::NoError)
    {
      out->DeepCopy(model);
      success = true;
    }

    reader->Delete();
  }
  else if (strcmp("obj", file_ext) == 0 || strcmp("OBJ", file_ext) == 0)
  {
    vtkOBJReader* reader = vtkOBJReader::New();
    reader->SetFileName(file_name);
    reader->Update();
    vtkPolyData* model = reader->GetOutput();

    if (reader->GetErrorCode() == vtkErrorCode::NoError)
    {
      out->DeepCopy(model);
      success = true;
    }

    reader->Delete();
  }
  else
    printf("File type '%s' is not supported. Supported file types are %s.\n", file_ext, m_list_of_supported_3d_formats);

  return success;
}

//=============================================================================================================

bool VtkIO::save(vtkPolyData* model, const char* file_name)
{
  vtkPolyDataWriter* writer = vtkPolyDataWriter::New();
    writer->SetInputData(model);

  if ( file_name == 0 )
  {
    char name[1024];
    printf("Type in file name (.vtk will be appended automatically):\n");

    if ( scanf("%s", name) != 1 )
    {
      printf("ERROR while reading user input.\n");
      writer->Delete();
      return false;
    }
    strcat(name, ".vtk");

    writer->SetFileName(name);
  }
  else
    writer->SetFileName(file_name);

  // Save the model
  writer->Write();

  bool result;

  // Check for errors
  if ( writer->GetErrorCode() != vtkErrorCode::NoError )
  {
    printf("ERROR: vtkPolyDataWriter could not save the model.\n");
    result = false;
  }
  else
  {
    result = true;
    if ( file_name == 0 )
      printf("'%s' successfully saved.\n", writer->GetFileName());
  }

  // Clean up
  writer->Delete();

  return result;
}

//=============================================================================================================

} /* namespace vtk */
