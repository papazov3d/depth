/*
 * VtkPolyModel.h
 *
 *  Created on: Jul 21, 2013
 *      Author: papazov
 */

#ifndef VTKPOLYMODEL_H_
#define VTKPOLYMODEL_H_

#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkAlgorithmOutput.h>
#include <vtkScalarsToColors.h>
#include <vtkProperty.h>
#include <vtkActor.h>
#include <vector>

namespace vtk
{

class VtkPolyModel
{
public:
  typedef std::pair<double,double> ScalarHuePair;

public:
  VtkPolyModel();
  VtkPolyModel(vtkAlgorithmOutput* poly_data_src);
  virtual ~VtkPolyModel();

  void reset();

  void copy_from(vtkPolyData* src){ this->get_poly_data()->DeepCopy(src);}
  
  vtkActor* get_actor(){ return m_actor;}
  vtkPolyDataMapper* get_mapper(){ return m_mapper;}
  vtkPolyData* get_poly_data(){ return m_mapper->GetInput();}
  vtkPolyData* get_poly_data() const { return m_mapper->GetInput(); }
  vtkScalarsToColors* get_lookup_table() const { return m_mapper->GetLookupTable(); }
  vtkPoints* get_points(){ return this->get_poly_data()->GetPoints();}
  inline vtkIdType get_number_of_points() const;

  void add_own_points(vtkIdType num_points = 0);

  void update(){ this->get_poly_data()->Modified();}

  /** Computes the normal of each triangle and saves it as a line in 'out'. */
  void compute_triangle_normals(vtkPolyData* out);

  void set_line_thickness(double value){ m_actor->GetProperty()->SetLineWidth(value);}
  void set_dot_size(double value){ m_actor->GetProperty()->SetPointSize(value); }
  void set_opacity(double value){ m_actor->GetProperty()->SetOpacity(value);}
  double get_opacity() const { return m_actor->GetProperty()->GetOpacity(); }
  void set_color(double r, double g, double b){ m_actor->GetProperty()->SetColor(r, g, b );}

  /** Creates a color transfer function which maps each scalar 'scalar_hue_pairs[i].first' to the hue value
   * 'scalar_hue_pairs[i].second'. */
  void set_color_mode_to_map_point_scalars_to_hue(const std::vector<ScalarHuePair>& scalar_hue_pairs);
  
  /** Creates a color transfer function which maps each scalar 'scalar_hue_pairs[i].first' to the hue value
   * 'scalar_hue_pairs[i].second'. */
  void set_color_mode_to_map_cell_scalars_to_hue(const std::vector<ScalarHuePair>& scalar_hue_pairs);

  void set_points(vtkPoints* points){ this->get_poly_data()->SetPoints(points);}
  void set_verts(vtkCellArray* verts){ this->get_poly_data()->SetVerts(verts); }
  void set_lines(vtkCellArray* lines){ this->get_poly_data()->SetLines(lines);}
  void set_polys(vtkCellArray* polys){ this->get_poly_data()->SetPolys(polys); }

  void backface_culling_on(){ m_actor->GetProperty()->BackfaceCullingOn();}
  void backface_culling_off(){ m_actor->GetProperty()->BackfaceCullingOff();}

  bool is_visible() const { return m_actor->GetVisibility() != 0;}
  void visibility_off(){ m_actor->VisibilityOff();}
  void visibility_on(){ m_actor->VisibilityOn();}
  void toggle_visibility(){ m_actor->SetVisibility(!m_actor->GetVisibility());}
  void toggle_backface_culling(){ m_actor->GetProperty()->SetBackfaceCulling(!m_actor->GetProperty()->GetBackfaceCulling());}

  void set_interpolation_to_flat(){ m_actor->GetProperty()->SetInterpolationToFlat();}
  void set_interpolation_to_gouraud();
  void set_interpolation_to_phong();

  void set_representation_to_surface(){ m_actor->GetProperty()->SetRepresentationToSurface();}
  void set_representation_to_wireframe(){ m_actor->GetProperty()->SetRepresentationToWireframe();}
  void set_representation_to_points(){ m_actor->GetProperty()->SetRepresentationToPoints();}

protected:
  /** Creates a color transfer function which maps each scalar 'scalar_hue_pairs[i].first' to the hue value
   * 'scalar_hue_pairs[i].second'. */
  void set_color_mode_to_map_scalars_to_hue(const std::vector<ScalarHuePair>& scalar_hue_pairs);

protected:
  vtkPolyDataMapper *m_mapper;
  vtkActor *m_actor;
};

//=== inline methods ==============================================================================================

inline vtkIdType VtkPolyModel::get_number_of_points() const
{
  vtkPolyData *poly_data = this->get_poly_data();

  if ( poly_data->GetPoints() )
    return poly_data->GetPoints()->GetNumberOfPoints();

  return 0;
}

//=================================================================================================================

} /* namespace vtk */

#endif /* VTKPOLYMODEL_H_ */
