/*
 * DepthFrameAverager.h
 *
 *  Created on: May 26, 2017
 *      Author: papazov
 */

#ifndef _CP_DEPTH_FRAME_AVERAGER_H_
#define _CP_DEPTH_FRAME_AVERAGER_H_

#include "DepthSensor.h"
#include "DepthFrame.h"
#include <iostream>

namespace cp
{

class DepthFrameAverager: public DepthSensor::DepthFrameReceiver
{
public:
  DepthFrameAverager(): m_num_frames_to_average(10){}
  virtual ~DepthFrameAverager(){}

  void set_number_of_frames_to_average(size_t n){ m_num_frames_to_average = n;}
  size_t get_number_of_frames_to_average() const { return m_num_frames_to_average;}
  
  /** Returns the number of frames saved so far. */
  size_t get_number_of_frames() const { return m_depth_frames.size();}

  const DepthFrame& get_average_depth_frame() const { return m_average_depth_frame;}

  inline void init_depth_processing();
  inline bool process_depth_frame(const DepthSense::DepthNode::NewSampleReceivedData& data);

protected:
  inline void average_collected_frames();
  inline void average_vertices();
  inline void average_depth_values();

protected:
  size_t m_num_frames_to_average;
  std::vector<DepthFrame> m_depth_frames;
  DepthFrame m_average_depth_frame;
};

//=== inline methods ========================================================================================

inline void DepthFrameAverager::init_depth_processing()
{
  m_depth_frames.clear();
}

//===========================================================================================================

inline bool DepthFrameAverager::process_depth_frame(const DepthSense::DepthNode::NewSampleReceivedData& data)
{
  if (m_depth_frames.size() >= m_num_frames_to_average)
    return false;

  // Save the current frame
  m_depth_frames.push_back(DepthFrame(data));

  // Shall we average?
  if (m_depth_frames.size() >= m_num_frames_to_average)
  {
    this->average_collected_frames();
    return false;
  }

  // We need more frames
  return true;
}

//===========================================================================================================

inline void DepthFrameAverager::average_collected_frames()
{
  if (m_depth_frames.empty())
    return;

  this->average_vertices();
  this->average_depth_values();
  
  // Set up other stuff
  m_average_depth_frame.set_stereo_camera_parameters(m_depth_frames[0].get_stereo_camera_parameters());
  m_average_depth_frame.set_width(m_depth_frames[0].get_width());
  m_average_depth_frame.set_height(m_depth_frames[0].get_height());
}

//===========================================================================================================

inline void DepthFrameAverager::average_vertices()
{
  if (m_depth_frames.empty())
    return;

  size_t num_vertices = m_depth_frames[0].get_vertices().size();
  std::vector<DepthSense::FPVertex> averaged_vertices(num_vertices);

  for (size_t idx = 0; idx < num_vertices; ++idx)
  {
    DepthSense::FPVertex avg_vertex(0, 0, 0);
    int num_valid_vertices = 0;

    for (auto& depth_frame : m_depth_frames)
    {
      const DepthSense::FPVertex& vertex = depth_frame.get_vertices()[idx];
      if (vertex.z <= 0)
        continue;
      
      avg_vertex.x += vertex.x;
      avg_vertex.y += vertex.y;
      avg_vertex.z += vertex.z;
      ++num_valid_vertices;
    }

    if (num_valid_vertices)
    {
      avg_vertex.x /= num_valid_vertices;
      avg_vertex.y /= num_valid_vertices;
      avg_vertex.z /= num_valid_vertices;
    }
    else
      avg_vertex.z = -2; // Invalid vertex

    averaged_vertices[idx] = avg_vertex;
  }

  m_average_depth_frame.set_vertices(averaged_vertices);
}

//===========================================================================================================

inline void DepthFrameAverager::average_depth_values()
{
  if (m_depth_frames.empty())
    return;

  size_t num_depth_values = m_depth_frames[0].get_depth_values().size();
  std::vector<float> averaged_depth(num_depth_values);

  for (size_t idx = 0; idx < num_depth_values; ++idx)
  {
    int num_valid_values = 0;
    float average_depth = 0;      

    for (auto& depth_frame : m_depth_frames)
    {
      float depth = depth_frame.get_depth_values()[idx];
      if (depth <= 0)
        continue;

      average_depth += depth;
      ++num_valid_values;
    }

    if (num_valid_values)
      average_depth /= num_valid_values;
    else
      average_depth = -2; // special DepthSenseSDK value

    averaged_depth[idx] = average_depth;
  }

  m_average_depth_frame.set_depth_values(averaged_depth);
}

//===========================================================================================================

} // namespace cp

#endif // _CP_DEPTH_FRAME_AVERAGER_H_
