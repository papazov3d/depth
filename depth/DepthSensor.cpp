/*
 * DepthSensor.cpp
 *
 *  Created on: May 24, 2017
 *      Author: papazov
 */

#include "DepthSensor.h"
#include <vector>
#include <exception>
#include <iostream>

#ifdef _MSC_VER
#include <windows.h>
#endif

using namespace std;
using namespace DepthSense;

namespace cp
{

DepthSensor* DepthSensor::g_depth_sensor = nullptr;

Context g_context;
DepthNode g_depth_node;
ColorNode g_color_node;
bool g_device_found = false;
bool g_all_set = false;
// Some settings for the depth sensor (you can set them in the Get() method)
bool g_enable_depth_map = true;
bool g_enable_vertices = false;
bool g_enable_confidence_map = false;
bool g_enable_floating_point_depth_map = false;
bool g_enable_floating_point_vertices = false;

//===========================================================================================================

void onNewDepthSample(DepthNode node, DepthNode::NewSampleReceivedData data)
{
  if (!g_all_set)
    return;

  DepthSensor::Get()->process_depth_frame(data);
}

//===========================================================================================================

void onNewColorSample(ColorNode node, ColorNode::NewSampleReceivedData data)
{
  if (!g_all_set)
    return;

  DepthSensor::Get()->process_color_frame(data);
}

//===========================================================================================================

void configureDepthNode()
{
  g_depth_node.newSampleReceivedEvent().connect(&onNewDepthSample);

  DepthNode::Configuration config = g_depth_node.getConfiguration();
  config.frameFormat = FRAME_FORMAT_QVGA;
  config.framerate = 25;
  config.mode = DepthNode::CAMERA_MODE_CLOSE_MODE;
  config.saturation = true;

  g_depth_node.setEnableDepthMap(g_enable_depth_map);
  g_depth_node.setEnableVertices(g_enable_vertices);
  g_depth_node.setEnableConfidenceMap(g_enable_confidence_map);
  g_depth_node.setEnableDepthMapFloatingPoint(g_enable_floating_point_depth_map);
  g_depth_node.setEnableVerticesFloatingPoint(g_enable_floating_point_vertices);

  try 
  {
    g_context.requestControl(g_depth_node, 0);
    g_depth_node.setCoordinateSystemType(DepthSense::COORDINATE_SYSTEM_TYPE_RIGHT_HANDED);
    g_depth_node.setConfiguration(config);
  }
  catch (ArgumentException& e) { cout << "Argument Exception: " << e.what() << endl;}
  catch (UnauthorizedAccessException& e) { cout << "Unauthorized Access Exception: " << e.what() << endl;}
  catch (IOException& e) { cout << "IO Exception: " << e.what() << endl;}
  catch (InvalidOperationException& e) { cout << "Invalid Operation Exception: " << e.what() << endl;}
  catch (ConfigurationException& e) { cout << "Configuration Exception: " << e.what() << endl;}
  catch (StreamingException& e) { cout << "Streaming Exception: " << e.what() << endl;}
  catch (TimeoutException&) { cout << "TimeoutException\n";}
}

//===========================================================================================================

void configureColorNode()
{
  g_color_node.newSampleReceivedEvent().connect(&onNewColorSample);

  ColorNode::Configuration config = g_color_node.getConfiguration();
  config.frameFormat = FRAME_FORMAT_VGA;
  config.compression = COMPRESSION_TYPE_MJPEG;
  config.powerLineFrequency = POWER_LINE_FREQUENCY_50HZ;
  config.framerate = 25;

  g_color_node.setEnableColorMap(true);

  try 
  {
    g_context.requestControl(g_color_node, 0);
    g_color_node.setConfiguration(config);
  }
  catch (ArgumentException& e){ cout << "Argument Exception: " << e.what() << endl;}
  catch (UnauthorizedAccessException& e){ cout << "Unauthorized Access Exception: " << e.what() << endl;}
  catch (IOException& e){ cout << "IO Exception: %s\n" << e.what() << endl;}
  catch (InvalidOperationException& e){ cout << "Invalid Operation Exception: " << e.what() << endl;}
  catch (ConfigurationException& e){ cout << "Configuration Exception: " << e.what() << endl;}
  catch (StreamingException& e){ cout << "Streaming Exception: " << e.what() << endl;}
  catch (TimeoutException&){ cout << "TimeoutException\n";}
}

//===========================================================================================================

void configureNode(Node node)
{
  if (node.is<DepthNode>() && !g_depth_node.isSet())
  {
    g_depth_node = node.as<DepthNode>();
    configureDepthNode();
    g_context.registerNode(node);
  }

  if (node.is<ColorNode>() && !g_color_node.isSet())
  {
    g_color_node = node.as<ColorNode>();
    configureColorNode();
    g_context.registerNode(node);
  }
}

//===========================================================================================================

void onNodeConnected(Device device, Device::NodeAddedData data)
{
  configureNode(data.node);
}

//===========================================================================================================

void onNodeDisconnected(Device device, Device::NodeRemovedData data)
{
  if (data.node.is<ColorNode>() && (data.node.as<ColorNode>() == g_color_node))
    g_color_node.unset();
  if (data.node.is<DepthNode>() && (data.node.as<DepthNode>() == g_depth_node))
    g_depth_node.unset();
}

//===========================================================================================================

void onDeviceConnected(Context context, Context::DeviceAddedData data)
{
  if (!g_device_found)
  {
    data.device.nodeAddedEvent().connect(&onNodeConnected);
    data.device.nodeRemovedEvent().connect(&onNodeDisconnected);
    g_device_found = true;
  }
}

//===========================================================================================================

void onDeviceDisconnected(Context context, Context::DeviceRemovedData data)
{
  g_device_found = false;
}

//===========================================================================================================

DepthSensor* DepthSensor::Get(bool enable_depth_map, bool enable_vertices, bool enable_confidence_map,
    bool enable_floating_point_depth_map, bool enable_floating_point_vertices)
{
  if (!g_depth_sensor)
  {
    // Save the settings
    g_enable_depth_map = enable_depth_map;
    g_enable_vertices = enable_vertices;
    g_enable_confidence_map = enable_confidence_map;
    g_enable_floating_point_depth_map = enable_floating_point_depth_map;
    g_enable_floating_point_vertices = enable_floating_point_vertices;
    // Create the new sensor
    g_depth_sensor = new DepthSensor();
  }

  return g_depth_sensor;
}

//===========================================================================================================

DepthSensor::DepthSensor()
: m_depth_frame_receiver(nullptr), m_color_frame_receiver(nullptr)
{
  g_context = Context::create("localhost");
  g_context.deviceAddedEvent().connect(&onDeviceConnected);
  g_context.deviceRemovedEvent().connect(&onDeviceDisconnected);

  // Get the list of currently connected devices
  vector<Device> devices = g_context.getDevices();

  // We are only interested in the first device
  if (!devices.empty())
  {
    g_device_found = true;
    
    devices[0].nodeAddedEvent().connect(&onNodeConnected);
    devices[0].nodeRemovedEvent().connect(&onNodeDisconnected);

    vector<Node> nodes = devices[0].getNodes();
    
    std::cout << "DepthSensor: found " << nodes.size() << " node(s)\n";
    
    for (size_t n = 0; n < nodes.size(); ++n)
    {
      std::cout << "  configuring node " << n + 1 << std::endl;
      configureNode(nodes[n]);
    }
    
    std::cout << "done\n";
  }

  g_all_set = true;
}

//===========================================================================================================

void DepthSensor::process_depth_frame(const DepthNode::NewSampleReceivedData& data)
{
  if (!m_send_data_to_depth_receiver && !m_send_data_to_color_receiver)
  {
    g_context.quit();
    return;
  }

  if (!m_send_data_to_depth_receiver)
    return;

  m_send_data_to_depth_receiver = m_depth_frame_receiver->process_depth_frame(data);
}

//===========================================================================================================

void DepthSensor::process_color_frame(const ColorNode::NewSampleReceivedData& data)
{
  if (!m_send_data_to_depth_receiver && !m_send_data_to_color_receiver)
  {
    g_context.quit();
    return;
  }

  if (!m_send_data_to_color_receiver)
    return;

  m_send_data_to_color_receiver = m_color_frame_receiver->process_color_frame(data);
}

//===========================================================================================================

void DepthSensor::start(DepthFrameReceiver* depth_frame_receiver, ColorFrameReceiver* color_frame_receiver)
{
  if (!g_all_set)
    return;

  // Handle the depth receiver
  m_depth_frame_receiver = depth_frame_receiver;
  if (m_depth_frame_receiver)
  {
    m_depth_frame_receiver->init_depth_processing();
    m_send_data_to_depth_receiver = true;
  }
  else
    m_send_data_to_depth_receiver = false;

  // Handle the color receiver
  m_color_frame_receiver = color_frame_receiver;
  if (m_color_frame_receiver)
  {
    m_color_frame_receiver->init_color_processing();
    m_send_data_to_color_receiver = true;
  }
  else
    m_send_data_to_color_receiver = false;

  g_context.startNodes();
  g_context.run(); // <--- does not return from here
  g_context.stopNodes();
}

//===========================================================================================================

} // namespace cp
