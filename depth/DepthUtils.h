/*
 * DepthUtils.h
 *
 *  Created on: June 14, 2017
 *      Author: papazov
 */

#ifndef _CP_DEPTH_UTILS_H_
#define _CP_DEPTH_UTILS_H_

#include <DepthSense.hxx>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

namespace cp
{

class DepthUtils
{
public:
  DepthUtils(){}
  virtual ~DepthUtils(){}

  static inline void reconstruct_3d_points(const std::vector<int16_t>& depth_values,
    const DepthSense::StereoCameraParameters& cam_params, std::vector<DepthSense::Vertex>& out);

  template<typename DepthType>
  static inline bool load_depth_data(const std::string& file_name, std::vector<DepthType>& depth_values);

  static inline bool save_stereo_camera_parameters(const std::string& file_name,
    const DepthSense::StereoCameraParameters& params);

  static inline bool load_stereo_camera_parameters(const std::string& file_name,
    DepthSense::StereoCameraParameters& p);

  static inline void print_stereo_camera_parameters(const DepthSense::StereoCameraParameters& p);
};

//=== inline methods ========================================================================================

inline void DepthUtils::reconstruct_3d_points(const std::vector<int16_t>& depth_values,
  const DepthSense::StereoCameraParameters& cam_params, std::vector<DepthSense::Vertex>& out)
{
  std::vector<DepthSense::Extended2DPoint> extended_points(depth_values.size());
  const int16_t* depth = depth_values.data();
  float fx, fy;
  int x, y, k;

  // Create the 2d points + depth (extended 2d points)
  for (y = 0, fy = 0, k = 0; y < cam_params.depthIntrinsics.height; ++y, fy += 1)
  {
    for (x = 0, fx = 0; x < cam_params.depthIntrinsics.width; ++x, fx += 1, ++depth)
    {
      if (*depth <= 0 || *depth == 32002)
        continue;

      extended_points[k].point.x = fx;
      extended_points[k].point.y = fy;
      extended_points[k].depth = *depth;
      ++k;
    }
  }

  extended_points.resize(k);
  out.resize(extended_points.size());

  // Perform the 3D reconstruction
  DepthSense::ProjectionHelper projector(cam_params);
  projector.get3DCoordinates(extended_points.data(), out.data(), /*#points=*/extended_points.size());
}

//===========================================================================================================

template<typename DepthType>
inline bool DepthUtils::load_depth_data(const std::string& file_name, std::vector<DepthType>& depth_values)
{
  // Open the file
  std::ifstream file(file_name, std::ifstream::binary);

  if (!file.is_open())
    return false;

  // Get length of the file:
  file.seekg(0, file.end); // go to the end
  int length_in_bytes = file.tellg(); // get the length
  file.seekg(0, file.beg); // go back to the beginning

  // Allocate memory for the whole file
  depth_values.resize(length_in_bytes / sizeof(DepthType));

  // Load the data
  file.read(reinterpret_cast<char*>(depth_values.data()), length_in_bytes);

  file.close();
  
  return true;
}

//===========================================================================================================

inline bool DepthUtils::save_stereo_camera_parameters(const std::string& file_name,
  const DepthSense::StereoCameraParameters& p)
{
  std::ofstream file(file_name);

  if (!file.is_open())
    return false;

  file << "# intrinsics format:\n"
    << "width height fx fy cx cy k1 k2 k3 p1 p2\n\n";

  file << "# depth intrinsics:\n"
    << p.depthIntrinsics.width << " "
    << p.depthIntrinsics.height << " "
    << p.depthIntrinsics.fx << " "
    << p.depthIntrinsics.fy << " "
    << p.depthIntrinsics.cx << " "
    << p.depthIntrinsics.cy << " "
    << p.depthIntrinsics.k1 << " "
    << p.depthIntrinsics.k2 << " "
    << p.depthIntrinsics.k3 << " "
    << p.depthIntrinsics.p1 << " "
    << p.depthIntrinsics.p2 << "\n\n";

  file << "# color intrinsics:\n"
    << p.colorIntrinsics.width << " "
    << p.colorIntrinsics.height << " "
    << p.colorIntrinsics.fx << " "
    << p.colorIntrinsics.fy << " "
    << p.colorIntrinsics.cx << " "
    << p.colorIntrinsics.cy << " "
    << p.colorIntrinsics.k1 << " "
    << p.colorIntrinsics.k2 << " "
    << p.colorIntrinsics.k3 << " "
    << p.colorIntrinsics.p1 << " "
    << p.colorIntrinsics.p2 << "\n\n";

  file << "# extrinsics format:\n"
    << "R_11 R_12 R_13 t_1\n"
    << "R_21 R_22 R_23 t_2\n"
    << "R_31 R_32 R_33 t_3\n\n";

  file << "# extrinsics:\n"
    << p.extrinsics.r11 << " " << p.extrinsics.r12 << "  " << p.extrinsics.r13 << " " << p.extrinsics.t1 << "\n"
    << p.extrinsics.r21 << " " << p.extrinsics.r22 << "  " << p.extrinsics.r23 << " " << p.extrinsics.t2 << "\n"
    << p.extrinsics.r31 << " " << p.extrinsics.r32 << "  " << p.extrinsics.r33 << " " << p.extrinsics.t3 << "\n";

  file.close();

  return true;
}

//===========================================================================================================

inline bool DepthUtils::load_stereo_camera_parameters(const std::string& file_name,
  DepthSense::StereoCameraParameters& p)
{
  std::ifstream file(file_name);

  if (!file.is_open())
    return false;

  std::string line;
  while (std::getline(file, line))
  {
    if (line.find("# depth intrinsics:") != std::string::npos)
    {
      // Get the next line which contains the depth intrinsics
      if (std::getline(file, line))
      {
        std::istringstream iss(line);
        DepthSense::IntrinsicParameters &i = p.depthIntrinsics; 
        iss >> i.width >> i.height >> i.fx >> i.fy >> i.cx >> i.cy >> i.k1 >> i.k2 >> i.k3 >> i.p1 >> i.p2;
      }
    }
    else if (line.find("# color intrinsics:") != std::string::npos)
    {
      // Get the next line which contains the color intrinsics
      if (std::getline(file, line))
      {
        std::istringstream iss(line);
        DepthSense::IntrinsicParameters &i = p.colorIntrinsics; 
        iss >> i.width >> i.height >> i.fx >> i.fy >> i.cx >> i.cy >> i.k1 >> i.k2 >> i.k3 >> i.p1 >> i.p2;
      }
    }
    else if (line.find("# extrinsics:") != std::string::npos)
    {
      DepthSense::ExtrinsicParameters &e = p.extrinsics;
      
      if (std::getline(file, line))
      {
        std::istringstream iss(line);
        iss >> e.r11 >> e.r12 >> e.r13 >> e.t1;
      }
      
      if (std::getline(file, line))
      {
        std::istringstream iss(line);
        iss >> e.r21 >> e.r22 >> e.r23 >> e.t2;
      }
      
      if (std::getline(file, line))
      {
        std::istringstream iss(line);
        iss >> e.r31 >> e.r32 >> e.r33 >> e.t3;
      }
    }
  }

  //DepthUtils::print_stereo_camera_parameters(p);

  file.close();
  return true;
}

//===========================================================================================================

inline void DepthUtils::print_stereo_camera_parameters(const DepthSense::StereoCameraParameters& p)
{
  std::cout << "# depth intrinsics:\n"
    << p.depthIntrinsics.width << " "
    << p.depthIntrinsics.height << " "
    << p.depthIntrinsics.fx << " "
    << p.depthIntrinsics.fy << " "
    << p.depthIntrinsics.cx << " "
    << p.depthIntrinsics.cy << " "
    << p.depthIntrinsics.k1 << " "
    << p.depthIntrinsics.k2 << " "
    << p.depthIntrinsics.k3 << " "
    << p.depthIntrinsics.p1 << " "
    << p.depthIntrinsics.p2 << "\n\n";

  std::cout << "# color intrinsics:\n"
    << p.colorIntrinsics.width << " "
    << p.colorIntrinsics.height << " "
    << p.colorIntrinsics.fx << " "
    << p.colorIntrinsics.fy << " "
    << p.colorIntrinsics.cx << " "
    << p.colorIntrinsics.cy << " "
    << p.colorIntrinsics.k1 << " "
    << p.colorIntrinsics.k2 << " "
    << p.colorIntrinsics.k3 << " "
    << p.colorIntrinsics.p1 << " "
    << p.colorIntrinsics.p2 << "\n\n";
    
  std::cout << "# extrinsics:\n"
    << p.extrinsics.r11 << " " << p.extrinsics.r12 << "  " << p.extrinsics.r13 << " " << p.extrinsics.t1 << "\n"
    << p.extrinsics.r21 << " " << p.extrinsics.r22 << "  " << p.extrinsics.r23 << " " << p.extrinsics.t2 << "\n"
    << p.extrinsics.r31 << " " << p.extrinsics.r32 << "  " << p.extrinsics.r33 << " " << p.extrinsics.t3 << "\n";
}

//===========================================================================================================

} // namespace cp

#endif // _CP_DEPTH_UTILS_H_
