/*
 * PlaneFitter.h
 *
 *  Created on: Juni 6, 2017
 *      Author: papazov
 */

#ifndef _CP_PLANE_FITTER_H_
#define _CP_PLANE_FITTER_H_

#include <DepthSense.hxx>
#include <Eigen/Dense>
#include <vector>

namespace cp
{

class PlaneFitter
{
public:
  /** The method computes the a, b and c which minimize sum_i (a*x_i + b*y_i + c - z_i)^2, where
   * (x_i, y_i, z_i) is the i-th point in 'points'. */
  static inline void fit_plane_2d(const std::vector<DepthSense::FPVertex>& points, float& a, float& b,
    float& c);
};

//=== inline methods ========================================================================================

inline void PlaneFitter::fit_plane_2d(const std::vector<DepthSense::FPVertex>& points, float& a, float& b,
  float& c)
{
  float x = 0, y = 0, z = 0, xx = 0, xy = 0, xz = 0, yy = 0, yz = 0;

  // Compute the coefficients of the linear system
  for (auto& p : points)
  {
    x += p.x;
    y += p.y;
    z += p.z;
    xx += p.x*p.x;
    xy += p.x*p.y;
    xz += p.x*p.z;
    yy += p.y*p.y;
    yz += p.y*p.z;
  }

  Eigen::Matrix3f A;
  A << xx, xy, x,
       xy, yy, y,
       x,  y,  static_cast<float>(points.size());

  Eigen::Vector3f u;
  u << xz, yz, z;

  Eigen::Vector3f sol = A.colPivHouseholderQr().solve(u);
  a = sol(0);
  b = sol(1);
  c = sol(2);
}

//===========================================================================================================

} // namespace cp

#endif // _CP_PLANE_FITTER_H_
