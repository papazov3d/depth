/*
 * DepthSensor.h
 *
 *  Created on: May 24, 2017
 *      Author: papazov
 */

#ifndef _CP_DEPTH_SENSOR_H_
#define _CP_DEPTH_SENSOR_H_

#include <DepthSense.hxx>

namespace cp
{

class DepthSensor
{
public:
  class DepthFrameReceiver
  {
  public:
    virtual void init_depth_processing() = 0;
    virtual bool process_depth_frame(const DepthSense::DepthNode::NewSampleReceivedData& data) = 0;
  };

  class ColorFrameReceiver
  {
  public:
    virtual void init_color_processing() = 0;
    virtual bool process_color_frame(const DepthSense::ColorNode::NewSampleReceivedData& data) = 0;
  };

protected:
  DepthSensor();
  virtual ~DepthSensor(){}

public:
  static DepthSensor* Get(
    bool enable_depth_map = true,
    bool enable_vertices = false,
    bool enable_confidence_map = false,
    bool enable_floating_point_depth_map = false,
    bool enable_floating_point_vertices = false);

  /** If you want to get only depth or color, set the other pointer to nullptr. */
  void start(DepthFrameReceiver* depth_frame_receiver, ColorFrameReceiver* color_frame_receiver);

  void process_depth_frame(const DepthSense::DepthNode::NewSampleReceivedData& data);
  void process_color_frame(const DepthSense::ColorNode::NewSampleReceivedData& data);

protected:
  static DepthSensor* g_depth_sensor;

  DepthFrameReceiver* m_depth_frame_receiver;
  ColorFrameReceiver* m_color_frame_receiver;

  bool m_send_data_to_depth_receiver;
  bool m_send_data_to_color_receiver;
};

} // namespace cp

#endif // _CP_DEPTH_SENSOR_H_
