/*
 * DepthFrame.h
 *
 *  Created on: May 29, 2017
 *      Author: papazov
 */

#ifndef _CP_DEPTH_FRAME_H_
#define _CP_DEPTH_FRAME_H_

#include <DepthSense.hxx>
#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>

namespace cp
{

class DepthFrame
{
public:
  DepthFrame(){}
  inline DepthFrame(const DepthSense::DepthNode::NewSampleReceivedData& src);
  virtual ~DepthFrame(){}

  inline void copy_from(const DepthSense::DepthNode::NewSampleReceivedData& src);

  /** Reconstructs and saves all 3d points of this depth map in 'out'. */
  inline void reconstruct_3d_points(std::vector<DepthSense::FPVertex>& out) const;

  /** Reconstructs the 3d point corresponding to the pixel (x, y) and saves the 3d coordinates in
   * (u, v, w). Returns true if the (x, y) pixel is valid and the reconstruction was successful
   * and false otherwise. */
  inline bool reconstruct_3d_point(int x, int y, DepthSense::FPVertex& out) const;

  void set_stereo_camera_parameters(const DepthSense::StereoCameraParameters& params){ m_camera_parameters = params;}
  const DepthSense::StereoCameraParameters& get_stereo_camera_parameters() const { return m_camera_parameters;}

  void set_confidence_values(const std::vector<int16_t>& src){ m_confidence_values = src;}
  const std::vector<int16_t>& get_confidence_values() const { return m_confidence_values;}

  void set_vertices(const std::vector<DepthSense::FPVertex>& src){ m_vertices = src;}
  const std::vector<DepthSense::FPVertex>& get_vertices() const { return m_vertices;}

  void set_depth_values(const std::vector<float>& src){ m_depth_values = src;}
  const std::vector<float>& get_depth_values() const { return m_depth_values;}
  
  float get_depth_value(int x, int y) const { return m_depth_values[y*m_width + x];}

  void set_width(int width){ m_width = width;}
  void set_height(int height){ m_height = height;}
  int get_width() const { return m_width;}
  int get_height() const { return m_height;}

protected:
  DepthSense::StereoCameraParameters m_camera_parameters;
  int m_width;
  int m_height;

  std::vector<int16_t> m_confidence_values;
  std::vector<DepthSense::FPVertex> m_vertices;
  std::vector<float> m_depth_values;
};

//=== inline methods ========================================================================================

inline DepthFrame::DepthFrame(const DepthSense::DepthNode::NewSampleReceivedData& src)
{
  this->copy_from(src);
}

//===========================================================================================================

inline void DepthFrame::copy_from(const DepthSense::DepthNode::NewSampleReceivedData& src)
{
  // Get the width and height of the image
  int32_t width, height;
  DepthSense::FrameFormat_toResolution(src.captureConfiguration.frameFormat, &width, &height);
  m_width = width;
  m_height = height;

  // Save the stereo parameters
  m_camera_parameters = src.stereoCameraParameters;

  // Copy the confidence values
  if (src.confidenceMap.size())
  {
    m_confidence_values.resize(src.confidenceMap.size());
    std::memcpy(m_confidence_values.data(), src.confidenceMap,
      src.confidenceMap.size()*sizeof(src.confidenceMap[0]));
  }

  // Copy the floating point vertices
  if (src.verticesFloatingPoint.size())
  {
    m_vertices.resize(src.verticesFloatingPoint.size());
    std::memcpy(m_vertices.data(), src.verticesFloatingPoint,
      src.verticesFloatingPoint.size()*sizeof(src.verticesFloatingPoint[0]));
  }

  // Copy the floating point depth
  if (src.depthMapFloatingPoint.size())
  {
    m_depth_values.resize(src.depthMapFloatingPoint.size());
    std::memcpy(m_depth_values.data(), src.depthMapFloatingPoint,
      src.depthMapFloatingPoint.size()*sizeof(src.depthMapFloatingPoint[0]));
  }
}

//===========================================================================================================

inline void DepthFrame::reconstruct_3d_points(std::vector<DepthSense::FPVertex>& out) const
{
  const float* depth_values = m_depth_values.data();
  int i, j, k = 0;
  float x, y;

  std::vector<DepthSense::FPExtended2DPoint> input_points(m_depth_values.size());

  // Create the 2d points + depth
  for (j = 0, y = 0; j < m_height; ++j, y += 1)
  {
    for (i = 0, x = 0; i < m_width; ++i, x += 1, ++depth_values)
    {
      if (*depth_values <= 0)
        continue;

      input_points[k].point.x = x;
      input_points[k].point.y = y;
      input_points[k].depth = *depth_values;
      ++k;
    }
  }

  input_points.resize(k);
  out.resize(input_points.size());

  // Perform the 3D reconstruction
  DepthSense::ProjectionHelper projector(m_camera_parameters);
  projector.get3DCoordinates(input_points.data(), out.data(), /*#points=*/input_points.size());
}

//===========================================================================================================

inline bool DepthFrame::reconstruct_3d_point(int x, int y, DepthSense::FPVertex& out) const
{
  DepthSense::FPExtended2DPoint p;
  p.depth = *(m_depth_values.data() + x + y*m_width);

  if (p.depth <= 0)
    return false;

  p.point.x = static_cast<float>(x);
  p.point.y = static_cast<float>(y);

  DepthSense::ProjectionHelper projector(m_camera_parameters);
  projector.get3DCoordinates(&p, &out, /*#points=*/1);
  
  return true;
}

//===========================================================================================================

} // namespace cp

#endif // _CP_DEPTH_FRAME_H_
